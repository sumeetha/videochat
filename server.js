var express = require('express');
//var bodyParser = require('body-parser')
var https = require('https');
var http = require('http');
var request = require('request');
var fs = require('fs');
var path = require('path');
var qs = require('querystring');
var s3BrowserDirectUpload = require('s3-browser-direct-upload');

var Q = require('q');

var app = express();
var srvPort = 3000;
var offset = 3600;
var username='';
var rtcAPI = 'service.xirsys.com';

var audioFilePath = __dirname+'/temp/';


var exec = require('child_process').exec;

var multer = require('multer');

// API Details - See Your Xirsys Account for Details
var ident = 'sumeetha'; //your username
var secret = '61669b9e-0659-11e8-b8e3-953124e4501c'; //your secret key
var domain = 'www.imirtc.com'; //your username
var application = 'imivideochat'; //your application name
//var room;
var room = 'default'; //your room name
var apiAdress = 'https://service.xirsys.com/';
var s3clientOptions = {
	  accessKeyId: 'AKIAJ7FH4SXISQI5VBNQ', // required
	  secretAccessKey: 'd6DNykqFDkKG2ED3yjuhlyJYnziQ3Zq2hIgF70FJ', // required
	  region: 'us-west-2', // required
	  signatureVersion: 'v4' // optional
	};
 
var allowedTypes = ['wav', 'flac', 'jpg', 'jpeg', 'png'];
 
var s3client = new s3BrowserDirectUpload(s3clientOptions, allowedTypes); // allowedTypes is optional

//SSL certificate.  Webrtc needs to be in a secure domain to do Audio / Video Sharing.
var opts = {
	// key:fs.readFileSync(path.join( __dirname, 'server.key')),
	// cert:fs.readFileSync(path.join( __dirname, 'server.crt'))
	//pfx: fs.readFileSync('localhost-webRTC.pfx'),
	passphrase: '0990'
};

var server = http.createServer(app).listen(srvPort);

console.log('Server on Port '+ srvPort);

var upload = multer({dest: __dirname + '/temp/uploads/'});

app.use(function(req, res, next){
	console.log('req: ',req.method,' for: ',req.url);
	next();
});
app.use(express.static('./public'));

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/speechtotext');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//Returns Secure token to connect to the service.
app.post('/webrtc/signal/token', function(req, res){
	console.log('GET SIGNAL TOKEN!');
	body = '';
	req.on('data', function(d){
		body += d;
	});
	req.on('end', function(){
		console.log(body)
		username = qs.parse(body).username;
		room=qs.parse(body).room;
		console.log('User request RTC Token username: ',username);

		var o = {
			ident: ident,
			secret: secret,
			domain: domain,
			application: application,
			room: room,
			secure: '1',
			username: username
		};
		bodyString = JSON.stringify(o);
		//console.log('User request RTC Token data: ',bodyString);
		options = {
			host: rtcAPI,
			path: '/signal/token',
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				'Content-Length':bodyString.length
			}
		};
		//get RTC token for user, # token back in response.
		https.request(options, function(httpres) {
			var str = '';
			httpres.on('data', function(data){ str += data; });
			httpres.on('error', function(e){ console.log('error: ',e); });
			httpres.on('end', function(){ 
				console.log("RTC Token! "+req.url,str); 
				res.writeHead(200, 'Content-Type', 'application/json');
				res.end(str);
			});
		}).write(bodyString);
	});
});

//Returns List of valid signaling servers that the clients can connect to.
app.get('/webrtc/signal/list',function(req, res){
	console.log("GET SIGNALS LIST!");
	console.log("For room: "+ room)
	var o = {
		ident: ident,
		secret: secret,
		domain: domain,
		application: application,
		room: room,
		secure: '1'
	};
	bodyString = JSON.stringify(o);
	//console.log('User request RTC List data: ',bodyString);

	options = {
		host: rtcAPI,
		path: '/signal/list',
		method: 'GET',
		headers: {
			'Content-Type':'application/json',
			'Content-Length':bodyString.length
		}
	};
	//get RTC server List for user.
	https.request(options, function(httpres) {
		var str = '';
		httpres.on('data', function(data){ str += data; });
		httpres.on('error', function(e){ console.log('error: ',e); });
		httpres.on('end', function(){ 
			console.log("RTC Server List!", str);
			res.writeHead(200, 'Content-Type', 'application/json');
			res.end(str);
		});
	}).write(bodyString);
});

//Returns a Valid ICE server setup to handle the WebRTC handshake and TURN connection if needed.
app.post('/webrtc/ice',function(req, res){
	console.log("GET ICE LIST!");
	var o = {
		ident: ident,
		secret: secret,
		domain: domain,
		application: application,
		room: room,
		secure: '1'
	};
	bodyString = JSON.stringify(o);

	console.log('User request ICE: ',bodyString);

	options = {
		host: rtcAPI,
		path: '/ice',
		method: 'POST',
		headers: {
			'Content-Type':'application/json',
			'Content-Length':bodyString.length
		}
	};
	//get RTC server List for user.
	https.request(options, function(httpres) {
		var str = '';
		httpres.on('data', function(data){ str += data; });
		httpres.on('error', function(e){ console.log('error: ',e); });
		httpres.on('end', function(){ 
			console.log("ICE String!", str);
			res.writeHead(200, 'Content-Type', 'application/json');
			res.end(str);
		});
	}).write(bodyString);
});

function saveBlobToFile(blob, encoding, fullfileName){
	var deferred = Q.defer();
	var buf = new Buffer(blob, encoding); // decode
	console.log("decoded blob from " + encoding)
	fs.writeFile(fullfileName, buf, function(err) {
		if(err) {
			console.log("err", err);
			deferred.reject(err);
		}
		else
			deferred.resolve(fullfileName);
	});
	return deferred.promise;
};

function ffmpegConverter(fileName){
	console.log("Started ffmpeg with:", fileName);
	var deferred = Q.defer();
	var cmd = 'ffmpeg -i temp/' + fileName + ' -ar 16000 -ac 1 temp/out_' + fileName;
	var newFileName = "out_" + fileName
	var newFullFileName = "temp/" + newFileName;
	console.log("COMMAND", cmd);
	exec(cmd, function(error, stdout, stderr) {
	  if(error)
        {
            console.log(error);
            deferred.reject(error);
        }
        else{
            console.log(stdout);
            deferred.resolve(newFileName);
        }
	  
	})
	return deferred.promise;
};

function saveFileToS3(fullfileName, fileName, bucket, fileType){
	var deferred = Q.defer();
	timeNow = Date.now();
	console.log(timeNow + ": Uploading "+ fileName +" to S3");
	var payload = {
			data: fs.createReadStream(fullfileName), // required
			key: fileName, // required
			bucket: bucket, // required
			extension: fileType, // optional (pass if You want to check with allowed extensions or set ContentType)
			acl: 'public-read', // optional
			expires: new Date(new Date(new Date().getTime() + (offset * 1000))).toISOString()
		};
	s3client.upload(payload, function(err, url) {
		if(err)
		{
			console.log(err);
			deferred.reject(err);
		}
		else{
			console.log("S3 url :"+ url)
			deferred.resolve(url);
		}
	});
	return deferred.promise;
};

function getTokenForTextToSpeech() {
	var deferred = Q.defer();

	var options = {
		method: 'POST',
		url: 'https://api.cognitive.microsoft.com/sts/v1.0/issueToken',
		headers: {
			'Ocp-Apim-Subscription-Key': '767e6d16c6c54330a1ce3bf1b3abcfee'
		}
	};

	var requestCallback = function(error, response, body) {
		if (!error) {
			console.log(body);
			deferred.resolve(body);
		}
		else {
			console.log('Error happened: '+ error);
			deferred.reject(error);
		}
	};

	request(options, requestCallback);

	return deferred.promise;
};

function convertTextToSpeech(text, language, gender, voice_ms, token) {
	// POST Token API: https://api.cognitive.microsoft.com/sts/v1.0/issueToken
	// Header for Token API: Ocp-Apim-Subscription-Key: 767e6d16c6c54330a1ce3bf1b3abcfee
	/*
	URL for TTS API (POST)
	https://speech.platform.bing.com/synthesize

	HEADERS for TTS API
		** // Header: Authorization: Bearer [Base64 access_token] // Convert access_token to base64
		headers = {"Content-type": "application/ssml+xml",
               "X-Microsoft-OutputFormat": "audio-16khz-64kbitrate-mono-mp3",
               "Authorization": "Bearer " + accesstoken,
               "X-Search-AppId": "07D3234E49CE426DAA29772419F436CA",
               "X-Search-ClientID": "1ECFAE91408841A480F00935DC390960",
               "User-Agent": "TTSForPython"}

	BODY
		** mp3_filename1 = bot_id+user_id+mp3_filename+str(randint(1,100000)) + '.mp3'
		** gender
		** lang
		** message
		body = "<speak version='1.0' xml:lang=" + "'" + lang + "'" + "><voice xml:lang=" + "'" + lang + "' " + "xml:gender=" + "'" + gender + "' " + "name='Microsoft Server Speech Text to Speech Voice ("+voice_ms+")'>" + message + "</voice></speak>"
	*/

	var deferred = Q.defer();

	// var accesstoken = new Buffer(token).toString('base64');
	var accesstoken = token;

	var payload ="<speak version='1.0' xml:lang=" + "'" + language + "'" + "><voice xml:lang=" + "'" + language + "' " + "xml:gender=" + "'" + gender + "' " + "name='Microsoft Server Speech Text to Speech Voice ("+voice_ms+")'>" + text + "</voice></speak>";

	console.log(payload);

	var options = {
		method: 'POST',
		url: 'https://speech.platform.bing.com/synthesize',
		headers: {
			"Content-Type": "application/ssml+xml",
			"X-Microsoft-OutputFormat": "audio-16khz-64kbitrate-mono-mp3",
			"Authorization": "Bearer " + accesstoken,
			"X-Search-AppId": "07D3234E49CE426DAA29772419F436CA",
			"X-Search-ClientID": "1ECFAE91408841A480F00935DC390960"
		},
		encoding: null,
		body: payload
	};

	console.log(options);
	function requestCallback(error, response, body) {
		if (!error) {
			console.log(response.read());
			// console.log(body);
			fs.writeFile('test.mp3', body, 'binary', function (err) {
		      // if (err) return callback(err);
		      // callback(null)
		      deferred.resolve('test.mp3');
		    });
		}
		else {
			console.log('Error happened: '+ error);
			deferred.reject(error);
		}
	}

	//send request
	request(options, requestCallback);

	return deferred.promise;	
};

function convertSpeechToText(url, language){ 
      var deferred = Q.defer();
	  console.log(Date.now() + ": calling speech to text api for "+ language);
	  var payload = { "file_uri": url,"language": language,"library": "google"};

	  var options = {
		method: 'POST',
		url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/speechtotext',
		headers: {
			'Content-Type': 'application/json'
		},
		json: payload
	  };

	  function requestCallback(error, response, body) {
		if (!error) {
			var info = JSON.parse(JSON.stringify(body));
			if(info.text) {
				console.log("Here is the speech to text output in " + language);
				console.log(info.text);
				text = info.text;
				deferred.resolve(text);	
			} else {
				console.log("Output was undefined", info);
				deferred.resolve(null);
			}
		}
		else {
			console.log('Error happened: '+ error);
			deferred.reject(error);
		}
	  }

	  //send request
	  request(options, requestCallback);

	  return deferred.promise;	  
};

function translateToText(inputString, targetLanguage){ 
      var deferred = Q.defer();
	  console.log(Date.now() + ": calling translation api for "+ targetLanguage);
	  var payload = { "text": inputString, "library": "google", "target":targetLanguage};
	  var options = {
		method: 'POST',
		url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/translate',
		headers: {
			'Content-Type': 'application/json'
		},
		json: payload
	  };

	  console.log("INSIDE TRANSLATION API");
	  console.log(payload);
	  console.log(options);

	  function requestCallback(error, response, body) {
		if (!error) {
			var info = JSON.parse(JSON.stringify(body));
			console.log("RECIEVED OUTPUT FROM TRANSLATE API: ", info);			
			if(info.text) {
				console.log("Here is the translation output in " + targetLanguage);
				console.log(info.text);
				var translatedText = info.text;
				deferred.resolve(translatedText);
			} else {
				console.log("The translated text output was undefined", info);
				deferred.resolve(null);
			}
		}
		else {
			console.log('Error happened: '+ error);
			deferred.reject(error);
		}
	  }

	  //send request
	  request(options, requestCallback);

	  return deferred.promise;	  
};

app.post('/api/uploadImageToS3', upload.single('image'), function(req, res){
	var body = '';
	console.log(req.body);
	console.log(req.file);

	saveFileToS3(req.file.path, req.file.filename + ".jpg", 'imirndpoc', 'jpg')
		.then(function(url) {
			res.json({url: url});
		});

	// res.json({"succes": true});
	// req.on('data', function(d){
	// 	body += d;
	// });
	// req.on('end', function(){
	// 	console.log(body)
	// 	console.log(qs.parse(body))
	// 	var blob = qs.parse(body).image;
	// 	var filename = qs.parse(body).filename;
	// 	console.log(blob);

	// 	var fullfileName="temp/" + (new Date()).getTime() + '.jpg';
	// 	var filename = (new Date()).getTime() + '.jpg';
	// 	if(blob) {
	// 		// saveBlobToFile(blob, 'base64', fullfileName)
	// 		var data = img.replace(/^data:image\/\w+;base64,/, "");
	// 		var buf = new Buffer(data, 'base64');
	// 		fs.writeFile('image.jpg', buf);
	// 		res.json({success: true});
	// 		// .then(function(){
	// 		// 	console.log("Calling saveFileToS3");
	// 		// 	return saveFileToS3(fullfileName, fileName, 'imirndpoc', 'jpg');
	// 		// })
	// 		// .then(function(url){
	// 		// 	res.json({url: url})
	// 		// })
	// 	} else {
	// 		res.json({"error": true});
	// 	}		
	// });
});

app.post('/api/createTranscript', function(req, res){
	console.log('SAVING BLOB!');
	var blob = '';
	var fullfileName='';
	var fileName='';
	var url='';
	var timestamp;
	var text='';
	var user='';
	var language='en-US';
	var body = '';
	req.on('data', function(d){
		body += d;
	});

	req.on('end', function(){
		//console.log(body)
		blob = qs.parse(body).blob;
		timestamp=qs.parse(body).time;
		user=qs.parse(body).user;
		language=qs.parse(body).language;
		var timeNow = Date.now();
		var newTime;
		var timediff = timeNow - timestamp;
		console.log(timestamp + ": Blob generated");
		console.log(timeNow + ": Blob received");
		console.log("Time taken to receive blob at server in millisec : "+ timediff);
		fileName = user + "_" + timestamp +".wav";
		fullfileName="temp/" + fileName;
		console.log("saving recording to "+ fullfileName);
		if(blob)
		{
			saveBlobToFile(blob, 'base64', fullfileName)
			.then(function(){
				console.log("Calling saveFileToS3");
				return saveFileToS3(fullfileName, fileName, 'imirndpoc', 'wav');
			})
			.then(function(url){
				if(url) {
						newTime = Date.now();
						timediff = newTime - timeNow;
						console.log(newTime + ": Uploaded to " + url);
						console.log("Time taken to upload to S3 in millisec : " + timediff);
						//uri = "https://s3.amazonaws.com/speakerdiarization/rishab_prabhjot_one_channel.wav";
						timeNow = Date.now();
						return convertSpeechToText(url, language);
					} else {
						// TODO: Add error condition.
						throw new Error('STT_ERROR');
					}
			})
			.then(function(text){
				newTime = Date.now();
				timediff = newTime - timeNow;
				var response = {'status': 'OK', 'url':url, 'user':user, 'text':text, 'time':timestamp};
				console.log(newTime + ": speech to text response received");
				console.log("Time taken for speech to text in millisec : " + timediff);
				return res.status(200).json(response);
			})
			.catch(function(error) {
							return res.status(500).json(error);
			});

		}
	});
});

app.post('/api/facerecognition', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'http://localhost:8000/api',
			json: { 'url' : url},
			headers: {
				'content-type': 'application/json'
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				if(!(body.indexOf("Error") > -1) && !(body.indexOf("error") > -1)){
				res.json(body);
				}
				else
					{
						res.json(null);
					}
			}
			else {
				console.log('Error happened: '+ error);
				res.json(null);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/emotion/image', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/emotion/image',
			json: { 'url' : url, 'library': 'azure' },
			headers: {
				'Ocp-Apim-Subscription-Key': '767e6d16c6c54330a1ce3bf1b3abcfee',
				'content-type': 'application/json'
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				res.json(body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/facerecognitionkairos', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'https://api.kairos.com/recognize',
			json: { "image" : url, "gallery_name":"IMIFaceRecDemo" },
			headers: {
				'app_id': '1b1f90e3',
				'app_key': '90509fa947a7019f55c9d7ffb5e276df',
				'content-type': 'application/json'
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				res.json(body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/vision', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/vision',
			json: { 'url' : url, 'library': 'azure' },
			headers: {
		 		'Ocp-Apim-Subscription-Key': '767e6d16c6c54330a1ce3bf1b3abcfee',
				'content-type': 'application/json'
			}
		};

		//var options = {
		//	method: 'POST',
		//	url: 'https://southeastasia.api.cognitive.microsoft.com/vision/v1.0/analyze?visualFeatures=Categories,Description,Color,Adult',
		//	json: { 'url' : url},
		//	headers: {
		//		'Ocp-Apim-Subscription-Key': 'df929424bdf741779a10494afce6991e',
		//		'content-type': 'application/json'
		//	}
		//};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				res.json(body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/objectdetection', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/vision',
			json: { 'url' : url, 'library': 'google' },
			headers: {
				'Ocp-Apim-Subscription-Key': '767e6d16c6c54330a1ce3bf1b3abcfee',
				'content-type': 'application/json'
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				res.json(body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/face/image', function(req, res) {
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var qs = JSON.parse(body);
		var url = qs.url;

		console.log(url);

		var options = {
			method: 'POST',
			url: 'http://appstaging-1357282334.us-west-2.elb.amazonaws.com/face/image',
			json: { 'url' : url, 'library': 'azure' },
			headers: {
				'Ocp-Apim-Subscription-Key': '767e6d16c6c54330a1ce3bf1b3abcfee',
				'content-type': 'application/json'
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				console.log(body);
				res.json(body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/texttospeech', function(req, res) {
	console.log("Came Inside Text to Speech");
	var body = '';
	req.on('data', function(d){
		body += d;
	});

	req.on('end', function(){
		qs = JSON.parse(body);
		gender = qs.gender;
		language = qs.language;
		text = qs.text;
		voice = qs.voice;

		console.log(gender,language, text, voice);

		getTokenForTextToSpeech()
			.then(function(token) {
				console.log(token);
				return convertTextToSpeech(text, language, gender, voice, token);
			})
			.then(function(data) {
				res.json(data);
			})
			.catch(function(err) {
				console.log(err);
			});
	});
});

app.post('/api/translate', function(req, res){
	console.log('SAVING BLOB!');
	var blob = '';
	var fullfileName='';
	var fileName='';
	var url='';
	var timestamp;
	var inputText='';
	var outputText='';
	var user='';
	var inputLanguage='en-IN';
	var targetLanguage='hi';
	var body = '';
	req.on('data', function(d){
		body += d;
	});

	req.on('end', function(){
		//console.log(body)
		blob = qs.parse(body).blob;
		timestamp=qs.parse(body).time;
		user=qs.parse(body).user;
		inputLanguage=qs.parse(body).inputLanguage;
		targetLanguage=qs.parse(body).targetLanguage;
		var timeNow = Date.now();
		var newTime;
		var timediff = timeNow - timestamp;
		console.log(timestamp + ": Blob generated");
		console.log(timeNow + ": Blob received");
		console.log("Time taken to receive blob at server in millisec : "+ timediff);
		fileName = user + "_" + timestamp +".wav";
		fullfileName="temp/" + fileName;
		console.log("saving recording to "+ fullfileName);
		if(blob)
		{
			saveBlobToFile(blob, 'base64', fullfileName)
				.then(function() {
					return saveFileToS3(fullfileName, fileName, 'imirndpoc', 'wav');
				})			
				.then(function(url) {
					if(url) {
						newTime = Date.now();
						timediff = newTime - timeNow;
						console.log(newTime + ": Uploaded to " + url);
						console.log("Time taken to upload to S3 in millisec : " + timediff);
						timeNow = Date.now();
						return convertSpeechToText(url, inputLanguage);
					} else {
						// TODO: Add error condition.
						throw new Error('STT_ERROR');
					}
				})
				.then(function(inputText) {
					newTime = Date.now();
					timediff = newTime - timeNow;
					console.log(newTime + ": speech to text output :"+ inputText);
					console.log("Time taken for speech to text in millisec : " + timediff);
					if(!inputText) {
						var response = {'status': 'OK', 'url':url, 'user':user, 'text':'', 'time':timestamp};
						return res.status(200).json(response);
					}
					else if(inputText && inputText.trim()=="") {
						var response = {'status': 'OK', 'url':url, 'user':user, 'text':'', 'time':timestamp};
						return res.status(200).json(response);
					} else {
						console.log("CALLING TRANSLATION TEXT API NOW");
						return translateToText(inputText, targetLanguage);
					}	
				})
				.then(function(outputText) {
					console.log("CAME INSIDE TRANLSATION API NOW");
					newTime = Date.now();
					timediff = newTime - timeNow;
					var response = {'status': 'OK', 'url':url, 'user':user, 'text':outputText, 'time':timestamp};
					console.log(newTime + ": translation output :"+ outputText);
					console.log("Time taken for translation in millisec : " + timediff);
					return res.status(200).json(response);
				})
				.catch(function(error) {
					if(error.message == 'STT_ERROR') {
						res.status(500).json({error: "Speech to Text gave no response."});
					} else {
						return res.status(500).json(error);
					}
				});				
		}
	});
});

app.post('/api/createVerificationProfile', function(req, res){
	console.log("Creating Profile for verification");
	var body = '';

	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var options = {
			method: 'POST',
			url: "http://localhost:5000/createProfileVerification", // Url for Python Api
			headers: {
				'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
				'content-type':'application/json'
			},
			json:{
				"locale":"en-us"
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				res.json(response.body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/createIdentificationProfile', function(req, res){
	console.log("Creating Profile for identification");
	var body = '';

	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {

		var options = {
			method: 'POST',
			url: "http://localhost:5000/createProfileIdentification", // Url for Python Api
			headers: {
				'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
				'content-type':'application/json'
			},
			json:{
				"locale":"en-us"
			}
		};

		var requestCallback = function(error, response, body) {
			if (!error) {
				res.json(response.body);
			}
			else {
				console.log('Error happened: '+ error);
				res.json(error);
			}
		};

		request(options, requestCallback);
	});
});

app.post('/api/enrollVerification', function(req, res){
	console.log("Sending Audio file for verification enrollment");
	var body = '';

	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var id = qs.parse(body).id; // The base64 encoded blob
		var fileName = qs.parse(body).filename;
		var blob = qs.parse(body).blob;
		var fullFileName = __dirname+'/temp/' + fileName;
		saveBlobToFile(blob, 'base64',fullFileName)
            .then(function(fullFileName){
            	console.log("Blob has been saved to: ", fullFileName);
                console.log("Calling ffmpeg");
                return ffmpegConverter(fileName); 
            })
            .then(function(newFileName){ // Got a properly converted audio file 
            	console.log("FFmpeg Completed");
            	var filePath = audioFilePath + newFileName;
            	var options = {
					method: 'POST',
					url: "http://localhost:5000/enrollProfileVerification", // Url for Python Api
					headers: {
						'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
						'content-type':'application/json'
					},
					json: {
						"id": id,
						"file_path": filePath,
						"force_short_audio": true
					}
				};
				var requestCallback = function(error, response, body) {
					if (!error) {
						res.json(response);
					}
					else {
						console.log('Error happened: '+ error);
						res.json(error);
					}
				};
				request(options, requestCallback);
			})
			.catch(function(error){
				console.log(error);
			});
	});
});

app.post('/api/enrollIdentification', function(req, res){
	console.log("Sending Audio file for identication enrollment");
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var id = qs.parse(body).id;
		var fileName = qs.parse(body).filename;
		var blob = qs.parse(body).blob;
		// var fullFileName = 'public/temp/' + fileName;
		var newFileName = 'out_' + fileName

	// saveBlobToFile(blob, 'base64',fullFileName)
 //        .then(function(fullFileName){
 //        	console.log("Blob has been saved to: ", fullFileName);
 //            console.log("Calling ffmpeg");
 //            return ffmpegConverter(fileName); 
 //        })
        // .then(function(newFileName){ // Got a properly converted audio file 
        	var filePath = audioFilePath + newFileName;
        	var options = {
				method: 'POST',
				url: 'http://localhost:5000/enrollProfileIdentification', // Url for Python Api
				headers: {
					'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
					'content-type':'application/json'
				},
				json: {
					"id": id,
					"file_path": filePath,
					"force_short_audio": true
				}
			};

			var requestCallback = function(error, response, body) {
				if (!error) {
					res.json(response.body);
				}
				else {
					console.log('Error happened: '+ error);
					res.json(error);
				}
			};
			request(options, requestCallback);
		// })
		// .catch(function(error){
		// 	console.log(error);
		// });

	});
});

app.post('/api/userIdentification', function(req, res){
	console.log("Sending Audio file for user Identification");
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var fileName = qs.parse(body).filename;
		var blob = qs.parse(body).blob;
		var fullFileName = __dirname+'/temp/' + fileName;

	saveBlobToFile(blob, 'base64',fullFileName)
        .then(function(fullFileName){
        	console.log("Blob has been saved to: ", fullFileName);
            console.log("Calling ffmpeg");
            return ffmpegConverter(fileName); 
        })
        .then(function(newFileName){ // Got a properly converted audio file 
        	var filePath = audioFilePath + newFileName;
        	var options = {
				method: 'POST',
				url: 'http://localhost:5000/identifyFile', // Url for Python Api
				headers: {
					'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
					'content-type':'application/json'
				},
				json: {
					"file_path": filePath,
					"force_short_audio": true
				}
			};

			var requestCallback = function(error, response, body) {
				if (!error) {
					res.json(response.body);
				}
				else {
					console.log('Error happened: '+ error);
					res.json(error);
				}
			};
			request(options, requestCallback);
		})
		.catch(function(error){
			console.log(error);
		});
	});
});

app.post('/api/userVerification', function(req, res){
	console.log("Sending Audio file for user Verification");
	var body = '';
	req.on('data', function(d) {
		body += d;
	});

	req.on('end', function() {
		var id = qs.parse(body).id;
		var fileName = qs.parse(body).filename;
		var blob = qs.parse(body).blob;
		// var fullFileName = 'public/temp/' + fileName;
		var newFileName = 'out_' + fileName

	// saveBlobToFile(blob, 'base64',fullFileName)
 //        .then(function(fullFileName){
 //        	console.log("Blob has been saved to: ", fullFileName);
 //            console.log("Calling ffmpeg");
 //            return ffmpegConverter(fileName); 
 //        })
 //        .then(function(newFileName){ // Got a properly converted audio file 
        	var filePath = audioFilePath + newFileName;
        	var options = {
				method: 'POST',
				url: 'http://localhost:5000/verifyFile', // Url for Python Api
				headers: {
					'Ocp-Apim-Subscription-Key': 'ee06e5723749410a979cebd0bf337a21',
					'content-type':'application/json'
				},
				json: {
					"id": id,
					"file_path": filePath,
					"force_short_audio": true
				}
			};

			var requestCallback = function(error, response, body) {
				if (!error) {
					res.json(response.body);
				}
				else {
					console.log('Error happened: '+ error);
					res.json(error);
				}
			};
			request(options, requestCallback)
		// })
		// .catch(function(error){
		// 	console.log(error);
		// });
	});	
});
