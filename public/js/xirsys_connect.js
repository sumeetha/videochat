// 'ident' and 'secret' should ideally be passed server-side for security purposes.
// If secureTokenRetrieval is true then you should remove these two values.

// Insecure method
/*var xirsysConnect = {
	secureTokenRetrieval : false,
	data : {
		domain : '< www.yourdomain.com >',
		application : 'default',
		room : 'default',
		ident : '< Your username (not your email) >',
		secret : '< Your secret API token >',
		secure : 1
	}
};*/

// Secure method
var xirsysConnect = {
	secureTokenRetrieval : true,//change this to true for authenticating from server
	server : '/webrtc/',//<-- Path To Server Auth (node server listens to this path)
	data : {
		domain : 'www.imirtc.com',
		application : 'imivideochat',
		//room : 'default',
		ident : 'sumeetha',
		//secret : '05fe8b30-5287-11e7-bedd-7b534bcfe6c6',
		secret : '61669b9e-0659-11e8-b8e3-953124e4501c',
		secure : 1
	}
};

