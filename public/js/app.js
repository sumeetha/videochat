var myApp=angular.module("myApp", ['ngRoute','ui.bootstrap','ui.select'] );
myApp.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "room.html",
		controller :"roomController"
    })
    .when("/room/:id", {
        templateUrl : "webrtc.html",
		controller : "rtcController"
	})
	   .when("/validate/:id", {
        templateUrl : "validate.html",
		controller : "validateController"
    });    
});


myApp.controller('validateController',function($scope, $routeParams, $http, $timeout, $q){

	//-------------------------------------------------------------------------
	var videoElement = document.querySelector('video');
	var audioInputSelect = document.getElementById('audioSource');
	var audioOutputSelect = document.getElementById('select#audioOutput');
	var videoSelect = document.getElementById('videoSource');
	var selectors = [audioInputSelect, videoSelect];

	$scope.tester=false;
	$scope.videoAccess=false;
	$scope.audioAccess=false;
	$scope.roomid = $routeParams.id;



	function gotDevices(deviceInfos) {
	// Handles being called several times to update labels. Preserve values.
	var values = selectors.map(function(select) {
		return select.value;
	});
	selectors.forEach(function(select) {
		while (select.firstChild) {
			select.removeChild(select.firstChild);
		}
	});
	for (var i = 0; i !== deviceInfos.length; ++i) {
		var deviceInfo = deviceInfos[i];
		var option = document.createElement('option');
		option.value = deviceInfo.deviceId;
		if (deviceInfo.kind === 'audioinput') {
			option.text = deviceInfo.label ||
			'microphone ' + (audioInputSelect.length + 1);
			audioInputSelect.appendChild(option);
		} 
		else if (deviceInfo.kind === 'videoinput') {
			option.text = deviceInfo.label || 'camera ' + (videoSelect.length + 1);
			videoSelect.appendChild(option);
		} else {
			console.log('Some other kind of source/device: ', deviceInfo);
		}
	}
	selectors.forEach(function(select, selectorIndex) {
		if (Array.prototype.slice.call(select.childNodes).some(function(n) {
			return n.value === values[selectorIndex];
		})) {
			select.value = values[selectorIndex];
		}
	});
	}
	navigator.mediaDevices.enumerateDevices();


	function gotStream(stream) {
		// $scope.videoAccess=true;
		$scope.tester=true;
		// $scope.audioAccess=true;
		window.stream = stream; // make stream available to console
		videoElement.srcObject = stream;
		videoElement.muted=true;
		// Refresh button list in case labels have become available
		return navigator.mediaDevices.enumerateDevices();
	}

	// function handleSuccessvideo(stream) {
	// 	$scope.videoAccess=true;
	// 	$scope.$apply();
	// 	var videoTracks = stream.getVideoTracks();
	// 	window.stream = stream; // make variable available to browser console
	// 	// video.srcObject = stream;
	// 	videoElement.srcObject = stream;
	// 	videoElement.muted=true;
	// 	return navigator.mediaDevices.enumerateDevices();
	// }

	// function handleSuccessaudio(stream) {
	// 	$scope.audioAccess=true;
	// 	$scope.$apply();
	// 	// var videoTracks = stream.getVideoTracks();
	// 	window.stream = stream; // make variable available to browser console
	// 	// video.srcObject = stream;
	// 	microphone.start();
	// 	return navigator.mediaDevices.enumerateDevices();
	// }


	// var start = function() {
	// 	if (window.stream) {
	// 		window.stream.getTracks().forEach(function(track) {
	// 			track.stop();
	// 		});
	// 	}
	// 	var audioSource = audioInputSelect.value;
	// 	var videoSource = videoSelect.value;
	// 	var constraints1 = {     
	// 		audio: {deviceId: audioSource ? {exact: audioSource} : undefined},
	// 		video: false
	// 	};

	// 	var constraints2 = {     
	// 		audio: false,
	// 		video: {deviceId: videoSource ? {exact: videoSource} : undefined}
	// 	};

	// 	var getMedia = getUserMedia;

	// 	getMedia(constraints1).
	// 	then(handleSuccessaudio).then(gotDevices)
	// 	.catch(handleError1);
	// 	getMedia(constraints2).
	// 	then(handleSuccessvideo).then(gotDevices)
	// 	.catch(handleError2);
	// }

	function handleSuccessvideo(stream) {
        $scope.videoAccess=true;
        $scope.audioAccess=true;
        $scope.$apply();
        var videoTracks = stream.getVideoTracks();
        window.stream = stream; // make variable available to browser console
        // video.srcObject = stream;
        videoElement.srcObject = stream;
        videoElement.muted=true;

        return navigator.mediaDevices.enumerateDevices();
    }
    // function handleSuccessaudio(stream) {
    //  $scope.audioAccess=true;
    //  $scope.$apply();
    //  // var videoTracks = stream.getVideoTracks();
    //  window.stream = stream; // make variable available to browser console
    //  // video.srcObject = stream;
    //  microphone.start();
    //  return navigator.mediaDevices.enumerateDevices();
    // }
    var start = function() {
        if (window.stream) {
            window.stream.getTracks().forEach(function(track) {
                track.stop();
            });
        }
        var audioSource = audioInputSelect.value;
        var videoSource = videoSelect.value;
        var constraints1 = {     
            audio: {deviceId: audioSource ? {exact: audioSource} : undefined},
            video: {deviceId: videoSource ? {exact: videoSource} : undefined}
        };
        // var constraints2 = {     
        //  audio: false,
        //  video: {deviceId: videoSource ? {exact: videoSource} : undefined}
		// };
		//getUserMedia = (getUserMedia || navigator.mediaDevices.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);
        
        navigator.mediaDevices.getUserMedia(constraints1).then(handleSuccessvideo).then(gotDevices).catch(handleError1);
        // getMedia(constraints2).
        // then(handleSuccessvideo).then(gotDevices)
        // .catch(handleError2);
    }

	start();

	audioInputSelect.onchange = start;
	//audioOutputSelect.onchange = changeAudioDestination;
	videoSelect.onchange = start;

	function handleError1(error) {
		$scope.audioAccess=false;
	}

	function handleError2(error) {
		$scope.videoAccess=false;
	};

});


myApp.controller('roomController',function($scope){
});


myApp.controller('rtcController', function ($scope, $routeParams, $http, $timeout, $q) {

		$('#login-guest-modal').modal('show');
		$scope.loggedIn = false;

		$scope.peerName = "__all__";

		var voiceUsernameEl = document.getElementById('voice-signup-username');
		var voiceLoginButtonEl = document.getElementById('login-btn');
		var username;

		$scope.audioStatus = true;
		$scope.videoStatus = true;
		$scope.recordingStatus = false;
		var remoteStream;
		var localStream;

		$scope.messageToCaller;
		$scope.peerNotSelected = true;
		$scope.peerSelected = false;
		var makingCall = false;
		$scope.newPeerName;
		$scope.takingCallFrom;
		$scope.takeCall = false;
		$scope.messageCount = 0;

		var messageAudio = new Audio('../media/messageping.mp3');
		var callAudio = new Audio('../media/calltune.mp3');

		$scope.mediaRecording = true;

		$scope.showTranscriptStartBtn = true;
		$scope.showTranslateStartBtn = true;
		$scope.showTranscriptStopBtn = false;
		$scope.showTranslateStopBtn = false;

		$scope.timer = null;
		$scope.cvTimer = null;

		var ctrackerLocal=null;
		var ctrackerRemote=null;
		$scope.stopCV = false;

		$scope.localCanvasTimer = null;
		$scope.remoteCanvasTimer =null;

		$scope.showVisualMode = true;
		$scope.showRecordBtn = true;
		$scope.showToggleAudioBtn = true;

		$scope.guestUser;

		$scope.profileId = {};

		$scope.counter = 0;

		$scope.localEmotionValues = [];
		$scope.remoteEmotionValues = [];
		$scope.allRemoteEmotionValues = [];
		$scope.allLocalEmotionValues = [];
		$scope.localUser;

		$scope.textToSpeechQueue = {};
		$scope.textToSpeechCount = 0;
		$scope.currentTextToSpeechOrder = 1;
		$scope.sounds_to_play = [];
		$scope.audioInterval = null;


		$scope.user = {};
		$scope.textToSpeechCount = 0;
		$scope.currentTextToSpeechOrder = 1;				
		$scope.sounds_to_play = [];				
		$scope.audioInterval = null;

		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("active");
		});

		$scope.localVideoClass = "local-split-box";
		$scope.remoteVideoClass = "remote-split-box";

		$scope.switchVideoDefault = function(){
			$scope.localVideoClass = "minor-box";
			$scope.remoteVideoClass = "major-box";

			$("#split-view-button").removeClass("active");
			$("#default-view-button").addClass("active");
		}

		$scope.switchVideoSplit = function(){
			$scope.localVideoClass = "local-split-box";
			$scope.remoteVideoClass = "remote-split-box";

			$("#default-view-button").removeClass("active");
			$("#split-view-button").addClass("active");
		}
		
		$scope.closeLoginGuestModal = function(){		
			$('#login-guest-modal').modal('hide');
		}

		$scope.closeVoiceSignupModal = function(){		
			$('#signup-modal').modal('hide');
		}

		$scope.closeVoiceSigninModal = function(){
			$('#login-modal').modal('hide');
		}

		var mediaRecorder;


	// Speaker Recognition

		var recordedAudioBase64 = null;
		var audioRecorder;
		var audioConstraints = {
			audio:true
		}
		var mediaConstraints = {
			audio:true,
			video:{
			minWidth: 1280,
				minHeight: 720,

				maxWidth: 1920,
				maxHeight: 1080
			}
		}
		$scope.locale = 'en-us';

		$scope.signUpStatus = null;
		$scope.enrollCount = 0;
		$scope.verify = true;
		$scope.verifyError = false;
		$scope.verified = false;
		$scope.enroll = true;
		$scope.enrolling = false;
		$scope.enrollError = false;
		$scope.enrolled = false;
		$scope.userProfile = {};
		$scope.speakerId = [];

		$('.record-button').addClass("notRec");

	// Code for Enrollment Phrases
		$scope.phrase = {id: 1, text:"I am going to make him an offer he cannot refuse"};
		$scope.enrollPhrases = [
			{id: 1, text:"I am going to make him an offer he cannot refuse"},
			{id: 2, text:"Houston we have had a problem"},
			{id: 3, text:"My voice is my passport verify me"},
			{id: 4, text:"Apple juice tastes funny after toothpaste"},
			{id: 5, text:"You can get in without your password"},
			{id: 6, text:"You can activate security system now"},
			{id: 7, text:"My voice is stronger than passwords"},
			{id: 8, text:"My password is not your business"},
			{id: 9, text:"My name is unknown to you"},
			{id: 10, text:"Be yourself everyone else is already taken"}
		];

		function addSpeakerId(verificationId, identificationId) {
			// Parse any JSON previously stored in allEntries
			var voiceUsername = voiceUsernameEl.value;
			console.log("Saving Username", voiceUsername);
			var existingIds = JSON.parse(localStorage.getItem("speakerIds"));
			if(existingIds == null) existingIds = [];
			var speakerId = {
				"username": voiceUsername,
				"verification_id": verificationId,
				"identification_id": identificationId
			};
			localStorage.setItem("speakerId", JSON.stringify(speakerId));
			// Save allEntries back to local storage
			existingIds.push(speakerId);
			localStorage.setItem("speakerIds", JSON.stringify(existingIds));
		};

		$scope.changeEnrollPhrase= function(phrase){
			console.log("Entered Change Phrase with", phrase);
			$('.enroll-phrase-display').find("h3").text('"' + phrase.text + '"');
		};

		$scope.changeVerifyPhrase= function(phrase){
			console.log("Entered Change Phrase with", phrase);
			$('.verify-phrase-display').find("h3").text('"' + phrase.text + '"');
		};

		function giveVerificationId(identificationId){
			var existingIds = JSON.parse(localStorage.getItem("speakerIds"));
			console.log(existingIds);
			for(i=0;i<existingIds.length;i++){
				if(existingIds[i].identification_id == identificationId){
					return existingIds[i].verification_id;
				}
			}
		};
	// Code for enrollment phrases

	// Function to capture audio on device
		function captureUserAudio(audioConstraints) {
			var deferred = Q.defer();

			console.log("Entered capture user audio");
			navigator.mediaDevices.getUserMedia(audioConstraints)
				.then(function(stream){ //SuccessCallback with audio Stream
					audioRecorder = new MediaStreamRecorder(stream);
					audioRecorder.stream = stream;
					audioRecorder.recorderType = StereoAudioRecorder;
					audioRecorder.mimeType = 'audio/wav'; // check this line for audio/wav 

					
					audioRecorder.ondataavailable = function(blob) {
						var blobURL = URL.createObjectURL(blob);
						console.log("Blob URL created");
						if(!$scope.recording){ // Make api calls after user stops recording
							console.log("Entered Completed Recording", $scope.recording);
							$('.loader').css("display","block");
							audioRecorder.saveAudio(blob)
							.then(function(response){
								console.log("Got the base 64 encoded audio response ", response);
								deferred.resolve(response);
							})
							.catch(function(error){
								console.log("An error occurred in saveAudio :" + error);
								deferred.resolve(error);
							})
						}
					};
					audioRecorder.start(10000);
				})
				.catch(function(error){
					deferred.resolve("Audio Capture Error", error);
				});
			return deferred.promise;
		};

		function captureUserMedia(mediaConstraints) {

			console.log("Entered capture user media");
			navigator.mediaDevices.getUserMedia(mediaConstraints)
				.then(function(stream){ //SuccessCallback with audio Stream
					mediaRecorder = new MediaStreamRecorder(stream);
					mediaRecorder.stream = stream;
					mediaRecorder.recorderType = MediaRecorderWrapper;
					mediaRecorder.mimeType = 'video/webm'; // check this line for audio/wav 

					
					mediaRecorder.ondataavailable = function(blob) {
						var blobURL = URL.createObjectURL(blob);
						console.log("Blob URL created");
						if(!$scope.mediaRecording){ // Make api calls after user stops recording
							console.log("Entered Completed Recording", $scope.recording);
							mediaRecorder.save(blob);
						}
					};
					mediaRecorder.start(10000000);
				})
				.catch(function(error){
					console.log(error);
				})
		};

		function firstEnrollment(locale){
			var deferred = $q.defer();
			var responses = [];
			captureUserAudio(audioConstraints)
			.then(function(response){
				responses.push(response);
				return createVerificationProfile(locale)
			})
			.then(function (response){
				console.log(response);
				responses.push(response);
				return createIdentificationProfile(locale)
			})
			.then(function(response){
				console.log(response);
				responses.push(response);
				deferred.resolve(responses);
			})
			.catch(function(error){
				console.log("Error in profile creation and audio generation");
			});
			return deferred.promise;
		};

	// Sign Up Process
		// Sign up Record Button On-Click
		$('#signup-record-button').click(function(){
			if($('#signup-record-button').hasClass('notRec')){ // not recording now
				$('#signup-record-button').removeClass("notRec");
				$('#signup-record-button').addClass("Rec");
				$('#signup-record-button').find("i").removeClass('fa-microphone');
				$('#signup-record-button').find("i").addClass('fa-microphone-slash');
				$scope.recording = true;

				if(!$scope.signUpStatus){ // New SignUp Process

					$scope.signUpStatus = 'firstEnrollment';

					firstEnrollment($scope.locale)
					.then(function(responses){
						console.log(responses);
						$scope.profileId.verificationId = responses[1].verification_profile_id;					
						$scope.profileId.identificationId = responses[2].identification_profile_id;
						console.log(responses[0]);
						console.log($scope.profileId);
				
						enrollSpeaker($scope.profileId, responses[0]);
					});
				}
				else{ // Continuing Signup Process
					captureUserAudio(audioConstraints)
						.then(function(response){ // Got response with the base 64 encoded blob
							enrollSpeaker($scope.profileId, response);
						})
						.catch(function(response){ // Error in getting response with encoded blob
							console.log("Audio Capture Error", error);
						});
				}

			}
			else{ //recording now
				$('#signup-record-button').removeClass("Rec");
				$('#signup-record-button').addClass("notRec");
				$('#signup-record-button').find("i").removeClass('fa-microphone-slash');
				$('#signup-record-button').find("i").addClass('fa-microphone');
				$scope.recording = false;
				audioRecorder.stop();
				audioRecorder.stream.stop();

			}
		});

		function createVerificationProfile(locale){
			var deferred = Q.defer();
			var data = {'locale': locale};
			$.ajax({
				type: 'POST',
				url: '/api/createVerificationProfile',
				data: data,
				success: function(response) {
					console.log("The final response of the verification profile api", response);
					deferred.resolve(response);
				},
				error: function(err){
					console.log("Enrollment Error");
				}
			})
			return deferred.promise;
		};

		function createIdentificationProfile(locale){
			var deferred = Q.defer();
			var data = {'locale': locale}; 
			$.ajax({
				type: 'POST',
				url: '/api/createIdentificationProfile',
				data: data ,
				success: function(response) {
					console.log("The final response of the identification profile api", response);
					deferred.resolve(response);
				},
				error: function(err){
					console.log("Enrollment Error");
				}
			})
			return deferred.promise;
		};

		function enrollSpeaker(profileId, audioObject){
			var deferred = $q.defer();
			$scope.enrollError = false;
			var verificationId = profileId.verificationId;
			var identificationId = profileId.identificationId;
			console.log("Entering with speaker status", $scope.signUpStatus);
			switch($scope.signUpStatus) {
				case 'firstEnrollment': //start enrollment
					verificationEnrollment(verificationId, audioObject)
					.then(function(apiResponse){ // Response from the microsoft api
						$('.loader').css("display","none");
						console.log(apiResponse);
						if(apiResponse.statusCode === 200){
							$scope.enrollCount += 1;
							$scope.signUpStatus = 'secondEnrollment';
							$scope.$apply();
							document.getElementById("enroll-progressbar").style.width = "33%";
							console.log("Enrollments Remaining", apiResponse.body.remaining_enrollments);
						}
						else if(apiResponse.statusCode === 500){
							alert("Couldn't catch that! Please Enroll again by speaking slowly and clearly towards the Microphone.");
							console.log("Error! Please Enroll Again");
							$scope.enrollError = true;
							$scope.$apply();
						}
						// TODO: add progress bar functions and alerts
					})
					.catch(function(e){
						console.log("Enrollment Step One Error",e);
						$scope.enrollError = true;
						$('.loader').css("display", "none");
					})
					break;
				case 'secondEnrollment': //enrollment started, in the second step now
					verificationEnrollment(verificationId, audioObject)
					.then(function(apiResponse){ // Response from the microsoft api
						console.log(apiResponse);
						$('.loader').css("display","none");
						if(apiResponse.statusCode === 200){
							$scope.enrollCount += 1;
							$scope.signUpStatus = 'finalEnrollment';
							console.log("Enrollments Remaining", apiResponse.body.remaining_enrollments);
							$scope.$apply();
							document.getElementById("enroll-progressbar").style.width = "66%";
							$("#enroll-progressbar").removeClass("progress-bar-info").addClass("progress-bar-warning");
						}
						else if(apiResponse.statusCode === 500){
							alert("Please speak closerto the Microphone and try again.");
							console.log("Error! Please Enroll Again");
							$scope.enrollError = true;
							$scope.$apply();
						}
						// TODO: add progress bar functions and alerts
					})
					.catch(function(e){
						$('.loader').css("display", "none");
						console.log("Enrollment Step Two Error",e);
					})
					break;
				case 'finalEnrollment': // third and final step to get enrolled
					verificationEnrollment(verificationId, audioObject)
					.then(function(apiResponse){
						console.log(apiResponse);
						if(apiResponse.statusCode === 200){
							$scope.signUpStatus = 'Verification Enrollment Completed';
							console.log("Enrollments Remaining", apiResponse.body.remaining_enrollments);
							$scope.$apply();
							return identificationEnrollment(identificationId, audioObject)	
							.then(function(apiResponse){ // Response from the microsoft api
							$('.loader').css("display","none");
							if(apiResponse.enrollment_status === "Enrolled"){
								$scope.enrollCount += 1; 
								$scope.signUpStatus = 'SignedUp';
								$scope.enrolled = true;
								console.log(apiResponse);
								document.getElementById("enroll-progressbar").style.width = "100%";
								$("#enroll-progressbar").removeClass("progress-bar-warning").addClass("progress-bar-success");
								// Add the Ids to local storage along with the username in the signup modal
								addSpeakerId(verificationId, identificationId);
								alert("Enrolled Successfully. Remember your phrase, next time you Sign In");
								$('#signup-modal').modal('hide');
								$('#login-modal').modal('show');
								$scope.$apply(); 
							}
							else {
								$scope.signUpStatus = "finalEnrollment";
								$scope.enrollError = true;
								$scope.$apply();
							}
							var finalStatus = {
								signupStatus: $scope.signUpStatus,
								enrollCount: $scope.enrollCount,
								enrolled: $scope.enrolled 
							}
							console.log("Final Status", finalStatus);
							// TODO: Add progressbar functions and alerts
						})
						.catch(function(error){
							$('.loader').css("display","none");
							console.log("Identification Enrollment",error);
							alert("Oh snap! An unexpected error occurred with enrollment. Please try again");
						})
						}
						else if(apiResponse.statusCode === 500){
							$('.loader').css("display","none");
							console.log("Error in verification Enrollment");
							$scope.enrollError = true;
							$scope.$apply();
						}
						else{
							$('.loader').css("display","none");
							alert("Oh snap! An unexpected error occurred with enrollment. Please try again");    		
						}
					console.log("Giving these to identification:", identificationId, audioObject);	    		
					
					})
					.catch(function(error){
						$('.loader').css("display","none");
						console.log("Verification Enrollment Error", error);
					})
					break;
			}
		};

		function verificationEnrollment(verificationId, audioObject){
			console.log(audioObject);
			var deferred = Q.defer();
			var data = {
				"id": verificationId,
				"blob": audioObject.blob,
				"filename":audioObject.filename,
				"time":audioObject.time
			}
			$.ajax({
				type: 'POST',
				url: '/api/enrollVerification',
				data: data,
				success: function(response) {
					console.log("The final response of the enrollment api", response);

					deferred.resolve(response);
				},
				error: function(err){
					console.log("Enrollment Error");
				}
			})
			return deferred.promise;
		};

		function identificationEnrollment(identificationId, audioObject){
			var deferred = Q.defer();
			var data = {
				"id": identificationId,
				"blob": audioObject.blob,
				"filename":audioObject.filename,
				"time":audioObject.time
			};
			$.ajax({
				type: 'POST',
				url: '/api/enrollIdentification',
				data: data,
				success: function(response) {
					console.log("The final response of the identification enrollment api", response);
					deferred.resolve(response);
				},
				error: function(err){
					console.log("Enrollment Error");
				}
			})
			return deferred.promise;
		};

		function getUsernameFromIds(recognisedIds){
			var identificationId = recognisedIds.identification_id;
			var verificationId = recognisedIds.verification_id;
			var existingData = JSON.parse(localStorage.getItem("speakerIds"));
			console.log(existingData);
			for(i=0;i<existingData.length;i++){
				if((existingData[i].identification_id == identificationId) && (existingData[i].verification_id == verificationId)){
					return existingData[i].username;
				}
			}
			alert("Unable to verify, please try again");
		};

		function voiceLogin(username){
			userLogin(username);
		}

		function userLogin (user){
			$(".loader").css("display","block");
			username = user;
			if (!username || username == '') {
				console.log("No username came");
				return ;
			}
			$scope.loggedIn = true;
			$('#login-modal').modal('hide');
			$scope.createRoom();
			var connectionProperties = xirsysConnect.data;
			connectionProperties.room=$scope.roomid;
			connectionProperties.username = username;
			connectionProperties.automaticAnswer = automaticAnswer;
			$('#sidebar-content').slideToggle(500);
			$scope.$apply(function(){
				$scope.username = user;
			})
			p.open(connectionProperties);
			//$scope.captureStream("localVideoEl");
			$scope.showDataOnCanvas();
			console.log('Logged into room :' + connectionProperties.room);
			$(".loader").css("display","none");
			// $scope.audioInterval = window.setInterval(function() {
			// 	$scope.playAudio();
			// }, 1000);
		};

	// Log In Process
		$('#login-record-button').click(function(){ 
			if($('#login-record-button').hasClass('notRec')){ // not recording now
				$('#login-record-button').removeClass("notRec");
				$('#login-record-button').addClass("Rec");
				$('#login-record-button').find("i").removeClass('fa-microphone');
				$('#login-record-button').find("i").addClass('fa-microphone-slash');
				$scope.recording = true;
				captureUserAudio(audioConstraints)
						.then(function(response){ // Got response with the base 64 encoded blob
							console.log("Captured Audio");
							speakerRecognition(response)
								.then(function(recognisedIds){
									$('.loader').css("display","none");
									console.log("RecognisedIds from Speaker recognition", recognisedIds);
									$scope.recognisedUsername = getUsernameFromIds(recognisedIds);
									console.log($scope.recognisedUsername);
									voiceLogin($scope.recognisedUsername);
								})
								.catch(function(error){
									$('.loader').css("display","none");
									alert("Could not verify. Please try again.");
									console.log("Error after response from Speaker Recognition",error);
								})
						})
						.catch(function(error){ // Error in getting response with encoded blob
							$('.loader').css("display","none");
							alert("Error in capturing audio. Please make sure your Mic is turned on.");
							console.log("Audio Capture Error", error);
						})
			}
			else{ //recording now
				$('#login-record-button').removeClass("Rec");
				$('#login-record-button').addClass("notRec");
				$('#login-record-button').find("i").removeClass('fa-microphone-slash');
				$('#login-record-button').find("i").addClass('fa-microphone');
				$scope.recording = false;
				audioRecorder.stop();
			}
		});


		function speakerRecognition(audioObject){
			console.log("Entered Speaker Recognition with: ",audioObject);
			var deferred = Q.defer();
			speakerIdentification(audioObject) ///First Identify the speaker
				.then(function(response){
						console.log("Identified Speaker", response.identified_speaker);
						if(response.identified_speaker === "00000000-0000-0000-0000-000000000000"){		
						$('.loader').css("display","none");		
						alert("Could not identify you. Please enroll yourself or try again.");		
					}		
					else{
						var identificationId =  response.identified_speaker;
						$scope.identifiedSpeaker = identificationId;
						$scope.identified = true; // TODO: add this according to the positive response of the api
						var verificationId = giveVerificationId(identificationId);
						$scope.verifiedSpeaker = verificationId;
						speakerVerification(verificationId, audioObject) //Now verify the speaker
						.then(function(response){
							
							if((response.confidence  ===  'High' || response.confidence === 'Normal') && response.verification_result == "Accept"){
								$scope.verified = true;
									console.log("Speaker has been verified.");
									var recognitionStatus = {
										"identification_id": $scope.identificationId,
										"verification_id": $scope.verificationId,
										"identification": $scope.identified,
										"verification" : $scope.verified
									};
									console.log("Recognition Status", recognitionStatus);
									var recognisedIds = {
										"identification_id": $scope.identifiedSpeaker,
										"verification_id": $scope.verifiedSpeaker
									};
									console.log(recognisedIds);
									deferred.resolve(recognisedIds);
							}
							else{
								$('.loader').css("display","none");							
								alert("Verification Failed. Please try again");
								console.log(response);
							}
							
						})
						.catch(function(error){
							$('.loader').css("display","none");
							console.log("Error in verification Api", error);
							deferred.reject(error);
						})
					
					}
					})	
				.catch(function(error){
					$('.loader').css("display","none");
					alert("Verification Failed. Please try again");
					console.log("Error in identification Api", error);
					deferred.reject(error);
				})
			return deferred.promise; // Return final reply after complete verification process
		};

		function speakerIdentification(audioObject){
			var deferred = Q.defer();
			var data = {
				"blob": audioObject.blob,
				"filename":audioObject.filename,
				"time":audioObject.time
			};
			$.ajax({
				type: 'POST',
				url: '/api/userIdentification',
				data: data,
				success: function(response) {
					console.log("The final response of the user identification api", response);
					deferred.resolve(response);
				},
				error: function(err){
					console.log("Identification Error");
				}
			})
			return deferred.promise;
		};

		function speakerVerification(verificationId, audioObject){
			var deferred = Q.defer();
			var data = {
				"id": verificationId,
				"blob": audioObject.blob,
				"filename":audioObject.filename,
				"time":audioObject.time
			};
			$.ajax({
				type: 'POST',
				url: '/api/userVerification',
				data: data,
				success: function(response) {
					console.log("The final response of the user verification api", response);
					deferred.resolve(response);
				},
				error: function(err){
					console.log("Verification Error");
				}
			})
			return deferred.promise;
		};

	// End of Speaker Recognition

			// $("#remote-canvas,#remote-visual-canvas,#remote-video-overlay").hover(function(){
			//     $('#remote-video-overlay').show().addClass('animated fadeIn')
			// },function(){
			//     $('#remote-video-overlay').hide();
			// });

			// $("#local-canvas,#local-visual-canvas,#local-video-overlay").hover(function(){
			//     $('#local-video-overlay').show().addClass('animated fadeIn');
			// },function(){
			//     $('#local-video-overlay').hide();
			// });

		// $('#sidebar-toggle').click(function(){
		// 	$('#sidebar').toggleClass('sidebar-open');	
		// });

		function incrementMessageCount(){
            if($('#message-popup').hasClass('message-popup-open')){
                // do nothing
            }
            else{
                $scope.$apply(function(){
					$scope.messageCount += 1;
				});
            }
        };
        function decrementMessageCount(){
            if($('#message-popup').hasClass('message-popup-open')){
                $scope.$apply(function(){
					$scope.messageCount = 0;
				});
            }
        }

		$scope.showSidebar = function(){
			$('#sidebar-content').slideToggle(300);
		}

		$('#message-bubble-button').click(function(){
			if($('#message-popup').hasClass('message-popup-open')){
				$('#message-popup').removeClass('message-popup-open');
			}else{
				$('#message-popup').addClass('message-popup-open');
				decrementMessageCount();
			}
			// $('#message-bubble-button').addClass('message-bubble-out');
		})

		$('#message-close-button').click(function(){
			$('#message-popup').removeClass('message-popup-open');
			// $('#message-bubble-button').removeClass('message-bubble-out');
		})

		var remoteVideoEl = document.getElementById('remote-video');
		var peersEl = document.getElementById('peers'),
			loginEl = document.getElementById('guest-login'),
			logOutEl = document.getElementById('log-out'),
			usernameEl = document.getElementById('guest-login-username'),
			usernameLabelEl = document.getElementById('username-label'),
			messageEl = document.getElementById('user-message'),
			sendMessageEl = document.getElementById('send-message'),
			messagesEl = document.getElementById('messages');
					
			// Getting references to page DOM for video calling.
			var callPeerEl = document.getElementById('call-peer'),
				hangUpEl = document.getElementById('hang-up'),
				localVideoEl = document.getElementById('local-video'),
				remoteVideoEl = document.getElementById('remote-video'),
				localFullScreenEl = document.getElementById('toggle-local-fullscreen'),
				remoteFullScreenEl = document.getElementById('toggle-remote-fullscreen');
			var automaticAnswer = false;
			
						// Create a p2p object. Pass a proxy server with your ident and
						// secret if you intend to connect securely.// Settings for video calling.
							
			$scope.username = '';
					
			var p = new $xirsys.p2p( (xirsysConnect.secureTokenRetrieval === true) ? xirsysConnect.server : null,
						{
							audio: true, 
							video: true
						},
						localVideoEl,
						remoteVideoEl
					);
			var bgCanvas = document.getElementById('local-canvas');
			var bgContext = bgCanvas.getContext('2d');
			var localCanvas1 = document.getElementById('local-visual-canvas1');
			var localContext1 = localCanvas1.getContext('2d');
			var localCanvas2 = document.getElementById('local-visual-canvas2');
			var localContext2 = localCanvas2.getContext('2d');
			var localCanvas3 = document.getElementById('local-visual-canvas3');
			var localContext3 = localCanvas3.getContext('2d');

			var bgRemoteCanvas = document.getElementById('remote-canvas');
			var bgRemoteContext = bgRemoteCanvas.getContext('2d');
			var remoteCanvas1 = document.getElementById('remote-visual-canvas1');
			var remoteContext1 = remoteCanvas1.getContext('2d');
			var remoteCanvas2 = document.getElementById('remote-visual-canvas2');
			var remoteContext2 = remoteCanvas2.getContext('2d');
			var remoteCanvas3 = document.getElementById('remote-visual-canvas3');
			var remoteContext3 = remoteCanvas3.getContext('2d');


			var video = document.getElementById('local-video');
			var remoteVideo = document.getElementById('remote-video');
			var mediaRecorder;

			var remoteVideoSection = document.getElementById('remote-video-section');

		
		$scope.drawEmotionChart = function(emotions, emotionContainer){
			$timeout(function(){
				console.log("Drawing emotionChart");
				var allEmotions = emotions;
				var chartOptions = $scope.chartOptions;
					for(var i=0; i<emotions.length;i++){
						// chartOptions["series"] = emotions[i];
						new Highcharts.chart(emotionContainer, chartOptions)
					}
			},1000)
		};

		var chart;

		$scope.alertWhenEmotionTabActive = function(event, index){
			var emotionContainer = document.getElementById('emotion-container');
			//$scope.drawEmotionChart($scope.allLocalEmotionValues, emotionContainer);
			var emotions = $scope.allLocalEmotionValues;
			var chartData = [];
			if(emotions){
				var chartOptions = $scope.chartOptions;
				for(var i=0;i<emotions.length;i++){
					chartData.push({"data":emotions[i]});
				}
				chartOptions["series"] = chartData;
				chart = new Highcharts.Chart(emotionContainer,chartOptions);
			}
		};	
		
		// $scope.getHighestEmotion = function(emotions){
		// 	$scope.emotionValues = 
		// 	console.log(emotions);
		// 	Object.keys(emotions) //get keys
		//       .sort() //sort them alphabetically
		//       .forEach(function(key, i) { //append each element of sorted array to $scope.emotionValues
		//           console.log(key, data[key]);
		//           $scope.emotionValues.push[data[key]];
		//        });
		// };
		

		$scope.chartOptions = {
			chart: {
			type: 'spline',
			plotBorderWidth: 1,
			zoomType: 'xy'
		},
		legend: {
			enabled: false
		},

		title: {
			text: 'Emotimeter'
		},

		xAxis: {
			gridLineWidth: 1,
			title: {
				text: 'Time'
			},
			type: 'datetime',
			labels:{
				formatter: function () {
						var date = new Date(this.value);
						return date.getMilliseconds();
				}
			}
		},

		yAxis: {
			title: {
				text: 'Emotions'
			},
			maxPadding: 0.2,
			// labels:{
			// 	format: '{value}'
			// },
			categories: [ "anger", "contempt","disgust","fear","happiness","neutral","sadness","surprise"],
			min:0,
			max:7
		},

		tooltip: {
			useHTML: true,
			headerFormat: '<table>',
			pointFormat: '<tr><th colspan="2"><h3>Emotions</h3></th></tr>' +
				'<tr><th>Time:</th><td>{point.x}</td></tr>' +
				'<tr><th>Emotion:</th><td>{point.name}</td></tr>' +
				'<tr><th>Confidence:</th><td>{point.z}%</td></tr>',
			footerFormat: '</table>',
			followPointer: true
		},

		// plotOptions: {
		//     series: {
		//         dataLabels: {
		//             enabled: true,
		//             format: '{point.name}'
		//         }
		//     }
		// },
		series : [{
			data:[]
			// [
			// 	{x:1,y:1,z:13.6,name:"happy"},
			// 	{x:2,y:2,z:33.6,name:"sad"},
			// 	{x:3,y:2,z:73.6,name:"angry"},
			// 	{x:4,y:4,z:63.6,name:"surprise"}
			// ]

				// [{x:122,y:5,z:35,name:"neutral"},{x:222,y:1,z:59,name:"anger"}]
			
			}]
		};
		

	// 	$scope.chartOptions = {

	//     chart: {
	//         type: 'bubble',
	//         plotBorderWidth: 1,
	//         zoomType: 'xy'
	//     },

	//     legend: {
	//         enabled: false
	//     },

	//     title: {
	//         text: 'Sugar and fat intake per country'
	//     },

	//     subtitle: {
	//         text: 'Source: <a href="http://www.euromonitor.com/">Euromonitor</a> and <a href="https://data.oecd.org/">OECD</a>'
	//     },

	//     xAxis: {
	//         gridLineWidth: 1,
	//         title: {
	//             text: 'Daily fat intake'
	//         },
	//         labels: {
	//             format: '{value} gr'
	//         },
	//         plotLines: [{
	//             color: 'black',
	//             dashStyle: 'dot',
	//             width: 2,
	//             value: 65,
	//             label: {
	//                 rotation: 0,
	//                 y: 15,
	//                 style: {
	//                     fontStyle: 'italic'
	//                 },
	//                 text: 'Safe fat intake 65g/day'
	//             },
	//             zIndex: 3
	//         }]
	//     },

	//     yAxis: {
	//         startOnTick: false,
	//         endOnTick: false,
	//         title: {
	//             text: 'Daily sugar intake'
	//         },
	//         labels: {
	//             format: '{value} gr'
	//         },
	//         maxPadding: 0.2,
	//         plotLines: [{
	//             color: 'black',
	//             dashStyle: 'dot',
	//             width: 2,
	//             value: 50,
	//             label: {
	//                 align: 'right',
	//                 style: {
	//                     fontStyle: 'italic'
	//                 },
	//                 text: 'Safe sugar intake 50g/day',
	//                 x: -10
	//             },
	//             zIndex: 3
	//         }]
	//     },

	//     tooltip: {
	//         useHTML: true,
	//         headerFormat: '<table>',
	//         pointFormat: '<tr><th colspan="2"><h3>{point.country}</h3></th></tr>' +
	//             '<tr><th>Fat intake:</th><td>{point.x}g</td></tr>' +
	//             '<tr><th>Sugar intake:</th><td>{point.y}g</td></tr>' +
	//             '<tr><th>Obesity (adults):</th><td>{point.z}%</td></tr>',
	//         footerFormat: '</table>',
	//         followPointer: true
	//     },

	//     plotOptions: {
	//         series: {
	//             dataLabels: {
	//                 enabled: true,
	//                 format: '{point.name}'
	//             }
	//         }
	//     },

	//     series: [{
	//         data: [
	//             { x: 95, y: 95, z: 13.8, name: 'BE', country: 'Belgium' },
	//             { x: 86.5, y: 102.9, z: 14.7, name: 'DE', country: 'Germany' },
	//             { x: 80.8, y: 91.5, z: 15.8, name: 'FI', country: 'Finland' },
	//             { x: 80.4, y: 102.5, z: 12, name: 'NL', country: 'Netherlands' },
	//             { x: 80.3, y: 86.1, z: 11.8, name: 'SE', country: 'Sweden' },
	//             { x: 78.4, y: 70.1, z: 16.6, name: 'ES', country: 'Spain' },
	//             { x: 74.2, y: 68.5, z: 14.5, name: 'FR', country: 'France' },
	//             { x: 73.5, y: 83.1, z: 10, name: 'NO', country: 'Norway' },
	//             { x: 71, y: 93.2, z: 24.7, name: 'UK', country: 'United Kingdom' },
	//             { x: 69.2, y: 57.6, z: 10.4, name: 'IT', country: 'Italy' },
	//             { x: 68.6, y: 20, z: 16, name: 'RU', country: 'Russia' },
	//             { x: 65.5, y: 126.4, z: 35.3, name: 'US', country: 'United States' },
	//             { x: 65.4, y: 50.8, z: 28.5, name: 'HU', country: 'Hungary' },
	//             { x: 63.4, y: 51.8, z: 15.4, name: 'PT', country: 'Portugal' },
	//             { x: 64, y: 82.9, z: 31.3, name: 'NZ', country: 'New Zealand' }
	//         ]
	//     }]

	// };

		// $scope.chartOptions = {
		//     title: {
		//         text: 'Emotimeter'
		//     },

		//     subtitle: {
		//         text: 'Emotion analyzer'
		//     },

		//     yAxis: {
		//         title: {
		//             text: '%'
		//         }
		//     },

		// 	 xAxis: {
		//         tickInterval: 5 
		// 	 },

		//     legend: {
		//         layout: 'vertical',
		//         align: 'right',
		//         verticalAlign: 'middle'
		//     },

		//     plotOptions: {
		//         series: {
		//             pointStart: 0
		//         }
		//     },

		//    series:[]
		// };

		$scope.transcriptMessages=[];
		$scope.translateMessages=[];

		$scope.myEmotions = []

		$scope.azureLanguageCodes = {
			languageOptions: [
			{id: 'ar-EG', name:'(ar-EG ,Hoda)'},
			{id: 'ar-SA', name:'(ar-SA ,Naayf)'},
			{id: 'ca-ES', name:'(ca-ES ,HerenaRUS)'},
			{id: 'cs-CZ', name:'(cs-CZ ,Vit)'},
			{id: 'da-DK', name:'(da-DK ,HelleRUS)'},
			{id: 'de-AT', name:'(de-AT ,Michael)'},
			{id: 'de-CH', name:'(de-CH ,Karsten)'},
			{id: 'de-DE', name:'(de-DE ,Hedda)'},
			{id: 'de-DE', name:'(de-DE ,HeddaRUS)'},
			{id: 'de-DE', name:'(de-DE ,Stefan)'},
			{id: 'el-GR', name:'(el-GR ,Stefanos)'},
			{id: 'en-AU', name:'(en-AU ,Catherine)'},
			{id: 'en-AU', name:'(en-AU ,HayleyRUS)'},
			{id: 'en-CA', name:'(en-CA ,Linda)'},
			{id: 'en-CA', name:'(en-CA ,HeatherRUS)'},
			{id: 'en-GB', name:'(en-GB ,Susan)'},
			{id: 'en-GB', name:'(en-GB ,HazelRUS)'},
			{id: 'en-GB', name:'(en-GB ,George)'},
			{id: 'en-IE', name:'(en-IE ,Shaun)'},
			{id: 'en-IN', name:'(en-IN ,Heera)'},
			{id: 'en-IN', name:'(en-IN ,PriyaRUS)'},
			{id: 'en-IN', name:'(en-IN ,Ravi)'},
			{id: 'en-US', name:'(en-US ,ZiraRUS)'},
			{id: 'en-US', name:'(en-US ,JessaRUS)'},
			{id: 'en-US', name:'(en-US ,BenjaminRUS)'},
			{id: 'es-ES', name:'(es-ES ,Laura)'},
			{id: 'es-ES', name:'(es-ES ,HelenaRUS)'},
			{id: 'es-ES', name:'(es-ES ,Pablo)'},
			{id: 'es-MX', name:'(es-MX ,HildaRUS)'},
			{id: 'es-MX', name:'(es-MX ,Raul)'},
			{id: 'fi-FI', name:'(fi-FI ,HeidiRUS)'},
			{id: 'fr-CA', name:'(fr-CA ,Caroline)'},
			{id: 'fr-CA', name:'(fr-CA ,HarmonieRUS)'},
			{id: 'fr-CH', name:'(fr-CH ,Guillaume)'},
			{id: 'fr-FR', name:'(fr-FR ,Julie)'},
			{id: 'fr-FR', name:'(fr-FR ,HortenseRUS)'},
			{id: 'fr-FR', name:'(fr-FR ,Paul)'},
			{id: 'he-IL', name:'(he-IL ,Asaf)'},
			{id: 'hi-IN', name:'(hi-IN ,Kalpana)'},
			{id: 'hi-IN', name:'(hi-IN ,Apollo)'},
			{id: 'hi-IN', name:'(hi-IN ,Hemant)'},
			{id: 'hu-HU', name:'(hu-HU ,Szabolcs)'},
			{id: 'id-ID', name:'(id-ID ,Andika)'},
			{id: 'it-IT', name:'(it-IT ,Cosimo)'},
			{id: 'ja-JP', name:'(ja-JP ,Ayumi)'},
			{id: 'ja-JP', name:'(ja-JP ,Ichiro)'},
			{id: 'ko-KR', name:'(ko-KR ,HeamiRUS)'},
			{id: 'nb-NO', name:'(nb-NO ,HuldaRUS)'},
			{id: 'nl-NL', name:'(nl-NL ,HannaRUS)'},
			{id: 'pl-PL', name:'(pl-PL ,PaulinaRUS)'},
			{id: 'pt-BR', name:'(pt-BR ,HeloisaRUS)'},
			{id: 'pt-BR', name:'(pt-BR ,Daniel)'},
			{id: 'pt-PT', name:'(pt-PT ,HeliaRUS)'},
			{id: 'ro-RO', name:'(ro-RO ,Andrei)'},
			{id: 'ru-RU', name:'(ru-RU ,Irina)'},
			{id: 'ru-RU', name:'(ru-RU ,Pavel)'},
			{id: 'sk-SK', name:'(sk-SK ,Filip)'},
			{id: 'sv-SE', name:'(sv-SE ,HedvigRUS)'},
			{id: 'th-TH', name:'(th-TH ,Pattara)'},
			{id: 'tr-TR', name:'(tr-TR ,SedaRUS)'},
			{id: 'zh-CN', name:'(zh-CN ,HuihuiRUS)'},
			{id: 'zh-CN', name:'(zh-CN ,Yaoyao)'},
			{id: 'zh-CN', name:'(zh-CN ,Kangkang)'},
			{id: 'zh-HK', name:'(zh-HK ,Tracy)'},
			{id: 'zh-HK', name:'(zh-HK ,TracyRUS)'},
			{id: 'zh-HK', name:'(zh-HK ,Danny)'},
			{id: 'zh-TW', name:'(zh-TW ,Yating)'},
			{id: 'zh-TW', name:'(zh-TW ,HanHanRUS)'},
			{id: 'zh-TW', name:'(zh-TW ,Zhiwei)'}
			],
			selectedLanguage:{id: 'hi-IN' ,name:'(hi-IN, Kalpana)'}
		}
	//transcription
		$scope.transcriptLanguage = {id:'en-IN'	,name:'English(India)'};
		$scope.transcriptLanguageOptions = [
			{id:'af-ZA'	,name:	'Afrikaans(SouthAfrica)'},
			{id:'id-ID'	,name:	'Indonesian(Indonesia)'},
			{id:'ms-MY'	,name:	'Malay(Malaysia)'},
			{id:'ca-ES'	,name:	'Catalan(Spain)'},
			{id:'cs-CZ'	,name:	'Czech(CzechRepublic)'},
			{id:'da-DK'	,name:	'Danish(Denmark)'},
			{id:'de-DE'	,name:	'German(Germany)'},
			{id:'en-AU'	,name:	'English(Australia)'},
			{id:'en-CA'	,name:	'English(Canada)'},
			{id:'en-GB'	,name:	'English(UnitedKingdom)'},
			{id:'en-IN'	,name:	'English(India)'},
			{id:'en-IE'	,name:	'English(Ireland)'},
			{id:'en-NZ'	,name:	'English(NewZealand)'	},
			{id:'en-PH'	,name:	'English(Philippines)'	},
			{id:'en-ZA'	,name:	'English(SouthAfrica)'	},
			{id:'en-US'	,name:	'English(UnitedStates)'	},
			{id:'es-AR'	,name:	'Spanish(Argentina)'	},
			{id:'es-BO'	,name:	'Spanish(Bolivia)'	},
			{id:'es-CL'	,name:	'Spanish(Chile)'	},
			{id:'es-CO'	,name:	'Spanish(Colombia)'	},
			{id:'es-CR'	,name:	'Spanish(CostaRica)'	},
			{id:'es-EC'	,name:	'Spanish(Ecuador)'	},
			{id:'es-SV'	,name:	'Spanish(ElSalvador)'	},
			{id:'es-ES'	,name:	'Spanish(Spain)'	},
			{id:'es-US'	,name:	'Spanish(UnitedStates)'	},
			{id:'es-GT'	,name:	'Spanish(Guatemala)'	},
			{id:'es-HN'	,name:	'Spanish(Honduras)'	},
			{id:'es-MX'	,name:	'Spanish(Mexico)'	},
			{id:'es-NI'	,name:	'Spanish(Nicaragua)'	},
			{id:'es-PA'	,name:	'Spanish(Panama)'	},
			{id:'es-PY'	,name:	'Spanish(Paraguay)'	},
			{id:'es-PE'	,name:	'Spanish(Peru)'	},
			{id:'es-PR'	,name:	'Spanish(PuertoRico)'	},
			{id:'es-DO'	,name:	'Spanish(DominicanRepublic)'	},
			{id:'es-UY'	,name:	'Spanish(Uruguay)'	},
			{id:'es-VE'	,name:	'Spanish(Venezuela)'	},
			{id:'eu-ES'	,name:	'Basque(Spain)'	},
			{id:'fil-PH',name:	'Filipino(Philippines)'	},
			{id:'fr-CA'	,name:	'French(Canada)'	},
			{id:'fr-FR'	,name:	'French(France)'	},
			{id:'gl-ES'	,name:	'Galician(Spain)'	},
			{id:'hr-HR'	,name:	'Croatian(Croatia)'	},
			{id:'zu-ZA'	,name:	'Zulu(SouthAfrica)'	},
			{id:'is-IS'	,name:	'Icelandic(Iceland)'	},
			{id:'it-IT'	,name:	'Italian(Italy)'	},
			{id:'lt-LT'	,name:	'Lithuanian(Lithuania)'	},
			{id:'hu-HU'	,name:	'Hungarian(Hungary)'	},
			{id:'nl-NL'	,name:	'Dutch(Netherlands)'	},
			{id:'nb-NO'	,name:	'NorwegianBokmal(Norway)'	},
			{id:'pl-PL'	,name:	'Polish(Poland)'	},
			{id:'pt-BR'	,name:	'Portuguese(Brazil)'	},
			{id:'pt-PT'	,name:	'Portuguese(Portugal)'	},
			{id:'ro-RO'	,name:	'Romanian(Romania)'	},
			{id:'sk-SK'	,name:	'Slovak(Slovakia)'	},
			{id:'sl-SI'	,name:	'Slovenian(Slovenia)'	},
			{id:'fi-FI'	,name:	'Finnish(Finland)'	},
			{id:'sv-SE'	,name:	'Swedish(Sweden)'	},
			{id:'vi-VN'	,name:	'Vietnamese(Vietnam)'	},
			{id:'tr-TR'	,name:	'Turkish(Turkey)'	},
			{id:'el-GR'	,name:	'Greek(Greece)'	},
			{id:'bg-BG'	,name:	'Bulgarian(Bulgaria)'	},
			{id:'ru-RU'	,name:	'Russian(Russia)'	},
			{id:'sr-RS'	,name:	'Serbian(Serbia)'	},
			{id:'uk-UA'	,name:	'Ukrainian(Ukraine)'	},
			{id:'he-IL'	,name:	'Hebrew(Israel)'	},
			{id:'ar-IL'	,name:	'Arabic(Israel)'	},
			{id:'ar-JO'	,name:	'Arabic(Jordan)'	},
			{id:'ar-AE'	,name:	'Arabic(UnitedArabEmirates)'	},
			{id:'ar-BH'	,name:	'Arabic(Bahrain)'	},
			{id:'ar-DZ'	,name:	'Arabic(Algeria)'	},
			{id:'ar-SA'	,name:	'Arabic(SaudiArabia)'	},
			{id:'ar-IQ'	,name:	'Arabic(Iraq)'	},
			{id:'ar-KW'	,name:	'Arabic(Kuwait)'	},
			{id:'ar-MA'	,name:	'Arabic(Morocco)'	},
			{id:'ar-TN'	,name:	'Arabic(Tunisia)'	},
			{id:'ar-OM'	,name:	'Arabic(Oman)'	},
			{id:'ar-PS'	,name:	'Arabic(StateofPalestine)'	},
			{id:'ar-QA'	,name:	'Arabic(Qatar)'	},
			{id:'ar-LB'	,name:	'Arabic(Lebanon)'	},
			{id:'ar-EG'	,name:	'Arabic(Egypt)'	},
			{id:'fa-IR'	,name:	'Persian(Iran)'	},
			{id:'hi-IN'	,name:	'Hindi(India)'	},
			{id:'th-TH'	,name:	'Thai(Thailand)'	},
			{id:'ko-KR'	,name:	'Korean(SouthKorea)'	},
			{id:'cmn-Hant-TW'	,name:	'Chinese,Mandarin(Traditional,Taiwan)'	},
			{id:'yue-Hant-HK'	,name:	'Chinese,Cantonese(Traditional,HongKong)'	},
			{id:'ja-JP'			,name:	'Japanese(Japan)'	},
			{id:'cmn-Hans-HK'	,name:	'Chinese,Mandarin(Simplified,HongKong)'	},
			{id:'cmn-Hans-CN'	,name:	'Chinese,Mandarin(Simplified,China)'	}
		];

	// translation
		$scope.translateFromLanguage = {id:	'en-IN'	,name:	'English(India)'};
		$scope.translateFromLanguageOptions= [
			{id:	'af-ZA'	,name:	'Afrikaans(SouthAfrica)'	},
			{id:	'id-ID'	,name:	'Indonesian(Indonesia)'	},
			{id:	'ms-MY'	,name:	'Malay(Malaysia)'	},
			{id:	'ca-ES'	,name:	'Catalan(Spain)'	},
			{id:	'cs-CZ'	,name:	'Czech(CzechRepublic)'	},
			{id:	'da-DK'	,name:	'Danish(Denmark)'	},
			{id:	'de-DE'	,name:	'German(Germany)'	},
			{id:	'en-AU'	,name:	'English(Australia)'	},
			{id:	'en-CA'	,name:	'English(Canada)'	},
			{id:	'en-GB'	,name:	'English(UnitedKingdom)'	},
			{id:	'en-IN'	,name:	'English(India)'	},
			{id:	'en-IE'	,name:	'English(Ireland)'	},
			{id:	'en-NZ'	,name:	'English(NewZealand)'	},
			{id:	'en-PH'	,name:	'English(Philippines)'	},
			{id:	'en-ZA'	,name:	'English(SouthAfrica)'	},
			{id:	'en-US'	,name:	'English(UnitedStates)'	},
			{id:	'es-AR'	,name:	'Spanish(Argentina)'	},
			{id:	'es-BO'	,name:	'Spanish(Bolivia)'	},
			{id:	'es-CL'	,name:	'Spanish(Chile)'	},
			{id:	'es-CO'	,name:	'Spanish(Colombia)'	},
			{id:	'es-CR'	,name:	'Spanish(CostaRica)'	},
			{id:	'es-EC'	,name:	'Spanish(Ecuador)'	},
			{id:	'es-SV'	,name:	'Spanish(ElSalvador)'	},
			{id:	'es-ES'	,name:	'Spanish(Spain)'	},
			{id:	'es-US'	,name:	'Spanish(UnitedStates)'	},
			{id:	'es-GT'	,name:	'Spanish(Guatemala)'	},
			{id:	'es-HN'	,name:	'Spanish(Honduras)'	},
			{id:	'es-MX'	,name:	'Spanish(Mexico)'	},
			{id:	'es-NI'	,name:	'Spanish(Nicaragua)'	},
			{id:	'es-PA'	,name:	'Spanish(Panama)'	},
			{id:	'es-PY'	,name:	'Spanish(Paraguay)'	},
			{id:	'es-PE'	,name:	'Spanish(Peru)'	},
			{id:	'es-PR'	,name:	'Spanish(PuertoRico)'	},
			{id:	'es-DO'	,name:	'Spanish(DominicanRepublic)'	},
			{id:	'es-UY'	,name:	'Spanish(Uruguay)'	},
			{id:	'es-VE'	,name:	'Spanish(Venezuela)'	},
			{id:	'eu-ES'	,name:	'Basque(Spain)'	},
			{id:	'fil-PH'	,name:	'Filipino(Philippines)'	},
			{id:	'fr-CA'	,name:	'French(Canada)'	},
			{id:	'fr-FR'	,name:	'French(France)'	},
			{id:	'gl-ES'	,name:	'Galician(Spain)'	},
			{id:	'hr-HR'	,name:	'Croatian(Croatia)'	},
			{id:	'zu-ZA'	,name:	'Zulu(SouthAfrica)'	},
			{id:	'is-IS'	,name:	'Icelandic(Iceland)'	},
			{id:	'it-IT'	,name:	'Italian(Italy)'	},
			{id:	'lt-LT'	,name:	'Lithuanian(Lithuania)'	},
			{id:	'hu-HU'	,name:	'Hungarian(Hungary)'	},
			{id:	'nl-NL'	,name:	'Dutch(Netherlands)'	},
			{id:	'nb-NO'	,name:	'NorwegianBokmal(Norway)'	},
			{id:	'pl-PL'	,name:	'Polish(Poland)'	},
			{id:	'pt-BR'	,name:	'Portuguese(Brazil)'	},
			{id:	'pt-PT'	,name:	'Portuguese(Portugal)'	},
			{id:	'ro-RO'	,name:	'Romanian(Romania)'	},
			{id:	'sk-SK'	,name:	'Slovak(Slovakia)'	},
			{id:	'sl-SI'	,name:	'Slovenian(Slovenia)'	},
			{id:	'fi-FI'	,name:	'Finnish(Finland)'	},
			{id:	'sv-SE'	,name:	'Swedish(Sweden)'	},
			{id:	'vi-VN'	,name:	'Vietnamese(Vietnam)'	},
			{id:	'tr-TR'	,name:	'Turkish(Turkey)'	},
			{id:	'el-GR'	,name:	'Greek(Greece)'	},
			{id:	'bg-BG'	,name:	'Bulgarian(Bulgaria)'	},
			{id:	'ru-RU'	,name:	'Russian(Russia)'	},
			{id:	'sr-RS'	,name:	'Serbian(Serbia)'	},
			{id:	'uk-UA'	,name:	'Ukrainian(Ukraine)'	},
			{id:	'he-IL'	,name:	'Hebrew(Israel)'	},
			{id:	'ar-IL'	,name:	'Arabic(Israel)'	},
			{id:	'ar-JO'	,name:	'Arabic(Jordan)'	},
			{id:	'ar-AE'	,name:	'Arabic(UnitedArabEmirates)'	},
			{id:	'ar-BH'	,name:	'Arabic(Bahrain)'	},
			{id:	'ar-DZ'	,name:	'Arabic(Algeria)'	},
			{id:	'ar-SA'	,name:	'Arabic(SaudiArabia)'	},
			{id:	'ar-IQ'	,name:	'Arabic(Iraq)'	},
			{id:	'ar-KW'	,name:	'Arabic(Kuwait)'	},
			{id:	'ar-MA'	,name:	'Arabic(Morocco)'	},
			{id:	'ar-TN'	,name:	'Arabic(Tunisia)'	},
			{id:	'ar-OM'	,name:	'Arabic(Oman)'	},
			{id:	'ar-PS'	,name:	'Arabic(StateofPalestine)'	},
			{id:	'ar-QA'	,name:	'Arabic(Qatar)'	},
			{id:	'ar-LB'	,name:	'Arabic(Lebanon)'	},
			{id:	'ar-EG'	,name:	'Arabic(Egypt)'	},
			{id:	'fa-IR'	,name:	'Persian(Iran)'	},
			{id:	'hi-IN'	,name:	'Hindi(India)'	},
			{id:	'th-TH'	,name:	'Thai(Thailand)'	},
			{id:	'ko-KR'	,name:	'Korean(SouthKorea)'	},
			{id:	'cmn-Hant-TW'	,name:	'Chinese,Mandarin(Traditional,Taiwan)'	},
			{id:	'yue-Hant-HK'	,name:	'Chinese,Cantonese(Traditional,HongKong)'	},
			{id:	'ja-JP'			,name:	'Japanese(Japan)'	},
			{id:	'cmn-Hans-HK'	,name:	'Chinese,Mandarin(Simplified,HongKong)'	},
			{id:	'cmn-Hans-CN'	,name:	'Chinese,Mandarin(Simplified,China)'	}	
		];

		$scope.translateToLanguage = {id:'hi',name:	'Hindi'	};
		$scope.translateToLanguageOptions = [
				{id:	'af'	,name:	'Afrikaans'	},
				{id:	'sq'	,name:	'Albanian'	},
				{id:	'am'	,name:	'Amharic'	},
				{id:	'ar'	,name:	'Arabic'	},
				{id:	'hy'	,name:	'Armenian'	},
				{id:	'az'	,name:	'Azeerbaijani'	},
				{id:	'eu'	,name:	'Basque'	},
				{id:	'be'	,name:	'Belarusian'	},
				{id:	'bn'	,name:	'Bengali'	},
				{id:	'bs'	,name:	'Bosnian'	},
				{id:	'bg'	,name:	'Bulgarian'	},
				{id:	'ca'	,name:	'Catalan'	},
				{id:	'ceb'	,name:	'Cebuano'	},
				{id:	'ny'	,name:	'Chichewa'	},
				{id:	'zh-CN'	,name:	'Chinese (Simplified)'	},
				{id:	'zh-TW'	,name:	'Chinese (Traditional)'	},
				{id:	'co'	,name:	'Corsican'	},
				{id:	'hr'	,name:	'Croatian'	},
				{id:	'cs'	,name:	'Czech'	},
				{id:	'da'	,name:	'Danish'	},
				{id:	'nl'	,name:	'Dutch'	},
				{id:	'en'	,name:	'English'	},
				{id:	'eo'	,name:	'Esperanto'	},
				{id:	'et'	,name:	'Estonian'	},
				{id:	'tl'	,name:	'Filipino'	},
				{id:	'fi'	,name:	'Finnish'	},
				{id:	'fr'	,name:	'French'	},
				{id:	'fy'	,name:	'Frisian'	},
				{id:	'gl'	,name:	'Galician'	},
				{id:	'ka'	,name:	'Georgian'	},
				{id:	'de'	,name:	'German'	},
				{id:	'el'	,name:	'Greek'	},
				{id:	'gu'	,name:	'Gujarati'	},
				{id:	'ht'	,name:	'Haitian Creole'	},
				{id:	'ha'	,name:	'Hausa'	},
				{id:	'haw'	,name:	'Hawaiian'	},
				{id:	'iw'	,name:	'Hebrew'	},
				{id:	'hi'	,name:	'Hindi'	},
				{id:	'hmn'	,name:	'Hmong'	},
				{id:	'hu'	,name:	'Hungarian'	},
				{id:	'is'	,name:	'Icelandic'	},
				{id:	'ig'	,name:	'Igbo'	},
				{id:	'id'	,name:	'Indonesian'	},
				{id:	'ga'	,name:	'Irish'	},
				{id:	'it'	,name:	'Italian'	},
				{id:	'ja'	,name:	'Japanese'	},
				{id:	'jw'	,name:	'Javanese'	},
				{id:	'kn'	,name:	'Kannada'	},
				{id:	'kk'	,name:	'Kazakh'	},
				{id:	'km'	,name:	'Khmer'	},
				{id:	'ko'	,name:	'Korean'	},
				{id:	'ku'	,name:	'Kurdish'	},
				{id:	'ky'	,name:	'Kyrgyz'	},
				{id:	'lo'	,name:	'Lao'	},
				{id:	'la'	,name:	'Latin'	},
				{id:	'lv'	,name:	'Latvian'	},
				{id:	'lt'	,name:	'Lithuanian'	},
				{id:	'lb'	,name:	'Luxembourgish'	},
				{id:	'mk'	,name:	'Macedonian'	},
				{id:	'mg'	,name:	'Malagasy'	},
				{id:	'ms'	,name:	'Malay'	},
				{id:	'ml'	,name:	'Malayalam'	},
				{id:	'mt'	,name:	'Maltese'	},
				{id:	'mi'	,name:	'Maori'	},
				{id:	'mr'	,name:	'Marathi'	},
				{id:	'mn'	,name:	'Mongolian'	},
				{id:	'my'	,name:	'Burmese'	},
				{id:	'ne'	,name:	'Nepali'	},
				{id:	'no'	,name:	'Norwegian'	},
				{id:	'ps'	,name:	'Pashto'	},
				{id:	'fa'	,name:	'Persian'	},
				{id:	'pl'	,name:	'Polish'	},
				{id:	'pt'	,name:	'Portuguese'	},
				{id:	'ma'	,name:	'Punjabi'	},
				{id:	'ro'	,name:	'Romanian'	},
				{id:	'ru'	,name:	'Russian'	},
				{id:	'sm'	,name:	'Samoan'	},
				{id:	'gd'	,name:	'Scots Gaelic'	},
				{id:	'sr'	,name:	'Serbian'	},
				{id:	'st'	,name:	'Sesotho'	},
				{id:	'sn'	,name:	'Shona'	},
				{id:	'sd'	,name:	'Sindhi'	},
				{id:	'si'	,name:	'Sinhala'	},
				{id:	'sk'	,name:	'Slovak'	},
				{id:	'sl'	,name:	'Slovenian'	},
				{id:	'so'	,name:	'Somali'	},
				{id:	'es'	,name:	'Spanish'	},
				{id:	'su'	,name:	'Sundanese'	},
				{id:	'sw'	,name:	'Swahili'	},
				{id:	'sv'	,name:	'Swedish'	},
				{id:	'tg'	,name:	'Tajik'	},
				{id:	'ta'	,name:	'Tamil'	},
				{id:	'te'	,name:	'Telugu'	},
				{id:	'th'	,name:	'Thai'	},
				{id:	'tr'	,name:	'Turkish'	},
				{id:	'uk'	,name:	'Ukrainian'	},
				{id:	'ur'	,name:	'Urdu'	},
				{id:	'uz'	,name:	'Uzbek'	},
				{id:	'vi'	,name:	'Vietnamese'	},
				{id:	'cy'	,name:	'Welsh'	},
				{id:	'xh'	,name:	'Xhosa'	},
				{id:	'yi'	,name:	'Yiddish'	},
				{id:	'yo'	,name:	'Yoruba'	},
				{id:	'zu'	,name:	'Zulu'	}
		];

		
		$scope.changeTranscriptLanguage = function(item){
			$scope.transcriptLanguage = item;
		}

		$scope.changeTranslationFrom = function(item){
			$scope.translateFromLanguage = item;
		}		

		$scope.changeTranslationTo = function(item){
			$scope.translateToLanguage = item;
		}


		var recordRTCOptions = {
		recorderType: "MediaStreamRecorder1",
		mimeType: 'video/webm', // or video/webm\;codecs=h264 or video/webm\;codecs=vp9
		audioBitsPerSecond: 128000,
		videoBitsPerSecond: 128000,
		bitsPerSecond: 512000 // if this line is provided, skip above two
		//increased bit rate to improve the quality of recording
		};

		$scope.startMediaRecorder = function(){
			captureUserMedia(mediaConstraints);
		};
		$scope.stopMediaRecorder = function(){
			$scope.mediaRecording = false;
		};

		var recordRTC;

		$scope.captureStream = function(streamSource){

			if(streamSource == "remoteVideoEl"){
				if(remoteVideoEl.srcObject){
						remoteStream = remoteVideoEl.srcObject;
					}
					else if(remoteVideoEl.mozSrcObject){
						remoteStream = remoteVideoEl.mozSrcObject;
					}
					else if(remoteVideoEl.src){
						remoteStream = remoteVideoEl.src;
					}
				console.log("RemoteStream =",remoteStream);
			}
			else if(streamSource == "localVideoEl")
			{
				if(localVideoEl.srcObject){
					localStream = localVideoEl.srcObject;
				}
				else if(localVideoEl.mozSrcObject){
					localStream = localVideoEl.mozSrcObject;
				}
				else if(localVideoEl.src){
					localStream = localVideoEl.src;
				}
				console.log("LocalStream =",localStream);
			}
		}
			
		$scope.startRecording = function(){
			$scope.counter = 1;
			var arrayOfStreams = [];
			console.log("INSIDE STARTRECORDING");
			try{
				$scope.captureStream("localVideoEl");
				$scope.captureStream("remoteVideoEl");
				//stream = remoteVideoEl.srcObject;
				
				// if(remoteVideoEl.srcObject){
				// 	remotestream = remoteVideoEl.srcObject;
				// }
				// else if(remoteVideoEl.mozSrcObject){
				// 	remoteStream = remoteVideoEl.mozSrcObject;
				// }
				// else if(remoteVideoEl.src){
				// 	remoteStream = remoteVideoEl.src;
				// }

				// if(localVideoEl.srcObject){
				// 	localStream = localVideoEl.srcObject;
				// }
				// else if(remoteVideoEl.mozSrcObject){
				// 	localStream = localVideoEl.mozSrcObject;
				// }
				// else if(remoteVideoEl.src){
				// 	localStream = localVideoEl.src;
				// }
				if (localStream)
				{
					arrayOfStreams.push(localStream);
				}
				if(remoteStream){
					arrayOfStreams.push(remoteStream);
				}
			}
			catch(err)
			{
				console.log('An error occurred with capturing remote stream');
			}
			if(arrayOfStreams){
				$scope.showRecordBtn = false;
				recordRTC = RecordRTC(arrayOfStreams, recordRTCOptions);
				console.log("STARTING VIDEO RECORDING");
				recordRTC.startRecording();
			}
		};

		$scope.stopRecording = function(){
			try{
				recordRTC.stopRecording(function () {
				//video.src = audioVideoWebMURL;

				//var recordedBlob = recordRTC.getBlob();
				//recordRTC.getDataURL(function(dataURL) { });
				});
			
				$scope.showRecordBtn = true;
				$scope.mediaRecording = false;
			}
			catch(error){
				console.log("An error occurred in stopRecording");
			}
		};

		$scope.toggleAudioRecording = function(){
			$scope.audioStatus = !$scope.audioStatus; //audio state changes from initially true to false and vice-versa
			$scope.toggleLocalAudio();
			//Add some function
		}


		$scope.toggleVideoRecording = function(){
			$scope.videoStatus = !$scope.videoStatus; //video state changes from initially true to false and vice-versa
			$scope.toggleLocalVideo();
			//Add some function
		}


		$scope.toggleRecordMedia = function(){
			$scope.recordingStatus = !$scope.recordingStatus;
			if ($scope.recordingStatus) {
			// $scope.startMediaRecorder();
			$scope.startRecording();

			} else {
				//$scope.stopMediaRecorder()
				$scope.stopRecording();
				$timeout(function(){
					$scope.saveRecording();
				}, 1000);
			}
		};

		$scope.saveRecording = function(){
			var blob = recordRTC.getBlob();
			console.log(blob);
			var blobURL = URL.createObjectURL(blob);
			console.log('<a href="' + blobURL + '">' + blobURL + '</a>');
			var fileName = $scope.username + '-' + Date.now() 
			if(blobURL !=""){
				console.log("saving recording");
				recordRTC.save(fileName, 'blob.webm');
			}
		};
			
		$scope.startTranscript = function () {
			console.log($scope.transcriptLanguage);
			$scope.showTranscriptStopBtn = true;
			$scope.showTranscriptStartBtn = false;
			$scope.showTranslateStartBtn = true;
			$scope.showTranslateStopBtn = false;
			try{
				mediaRecorder.stop();
			}
			catch(error){
				//console.log(error);
			}
				var stream;				
				try{
					//stream = remoteVideoEl.srcObject;
					if(localVideoEl.srcObject){
						stream = localVideoEl.srcObject;
					}
					else if(localVideoEl.mozSrcObject){
						stream = localVideoEl.mozSrcObject;
					}
					else if(localVideoEl.src){
						stream = localVideoEl.src;
					}
				}
				catch(err)
				{
					console.log('An error occurred with capturing remote stream');
				}
				if(stream !=null)
				{
					var blobURL="";
					mediaRecorder = new MediaStreamRecorder(stream);
					mediaRecorder.mimeType = 'audio/wav'; // check this line for audio/wav
					mediaRecorder.ondataavailable = function (blob) {
						blobURL = URL.createObjectURL(blob);
						console.log('<a href="' + blobURL + '">' + blobURL + '</a>');
						if(blobURL !=""){
						//mediaRecorder.save(blobURL, 'blob.wav');
						console.log("calling mediaRecorder for "+ $scope.transcriptLanguage.id);
						mediaRecorder.convertAudioToText(blob, username, $scope.transcriptLanguage.id, 'blob.wav')
							.then(function(message){
								$scope.$apply(function(){
								console.log('Text =' + message);
								if(message.text)
								{
									$scope.transcriptMessages.push(message);
									$scope.notifyPeer(message, 'transcript');
								}
								});
							})
							.catch(function(error){
								console.log("An error occurred in app.js :" + error);
							})
						}
					};
					mediaRecorder.start(10000);
				}	
		};
		
		$scope.stopTranscript = function(){
			$scope.showTranscriptStartBtn = true;
			$scope.showTranscriptStopBtn = false;
			mediaRecorder.stop();
		};

		$scope.getTranscript = function() {
			console.log("CAME INSIDE TRANSCRIPT");
		};

		$scope.stopTranslate = function () {
			$scope.showTranslateStartBtn = true;
			$scope.showTranslateStopBtn = false;
			mediaRecorder.stop();
		};

		$scope.translateMessage = function (file, username, inputLanguage, targetLanguage, fileName) {
			if (!file) {
				throw 'Blob object is required.';
			}

			if (!file.type) {
				try {
					file.type = 'video/webm';
				} catch (e) {}
			}

			var fileExtension = (file.type || 'video/webm').split('/')[1];

			if (fileName && fileName.indexOf('.') !== -1) {
				var splitted = fileName.split('.');
				fileName = splitted[0];
				fileExtension = splitted[1];
			}

			// converts blob to base64
			var blobToBase64 = function(file, cb) {
			var reader = new FileReader();
			reader.onload = function() {
				var dataUrl = reader.result;
				var base64 = dataUrl.split(',')[1];
				cb(base64);
			};
			reader.readAsDataURL(file);
			};
			var deferred = Q.defer();
			var audiofileURL='';
			var text='';
			var user='';
			var time = Date.now();
			var formattedTime;
			blobToBase64(file, function(base64){ // encode
			var update = {'blob': base64, 'time': time, 'user':username, 'inputLanguage':inputLanguage, "targetLanguage":targetLanguage};
			console.log("sending blob to translate from "+ inputLanguage + " to " + targetLanguage);
			$.ajax({
			type: 'post',
			url: '/api/translate',
			data: update,
			success: function(response) {
				console.log("successfully saved audio file");
				audiofileURL=response.url;
				text = response.text;
				user = response.user;
				//time = Number(response.time);
				formattedTime = new Date(time);
				deferred.resolve({"text":text, "user":user, "time": time, "formattedTime":time, "type": "transcript"});
				console.log("File saved at :"+ audiofileURL);
				console.log("Translation output :" + text);
				var timeNow = Date.now();
				var diff = timeNow - time;
				console.log("Total time taken for audio translation in milliseconds :" + diff);
				//convertSpeechToText("https://s3.amazonaws.com/speakerdiarization/rishab_prabhjot_one_channel.wav");
				},
			error: function(err){
				console.log("an error occurred with saving the audio file");
				(console.error || console.log).call(console, err.stack || err);
				deferred.resolve(err);
			}
			});
			});
			return deferred.promise;
		};

		$scope.startTranslate = function () {
			$scope.showTranslateStartBtn = false;
			$scope.showTranslateStopBtn = true;
			$scope.showTranscriptStartBtn = true;
			$scope.showTranscriptStopBtn = false;
			try{
				mediaRecorder.stop();
			}
			catch(error){
				//console.log(error);
			}
				//starttranscriptBtn.style.visibility = 'visible';
				//stoptranscriptBtn.style.visibility = 'hidden';
				var stream;				
				try{
					//stream = remoteVideoEl.srcObject;
					if(localVideoEl.srcObject){
						stream = localVideoEl.srcObject;
					}
					else if(localVideoEl.mozSrcObject){
						stream = localVideoEl.mozSrcObject;
					}
					else if(localVideoEl.src){
						stream = localVideoEl.src;
					}
				}
				catch(err)
				{
					console.log('An error occurred with capturing local stream');
				}
				if(stream !=null)
				{
					//$scope.toggleAudio(stream);
					var blobURL="";
					mediaRecorder = new MediaStreamRecorder(stream);
					mediaRecorder.mimeType = 'audio/wav'; // check this line for audio/wav
					mediaRecorder.ondataavailable = function (blob) {
						blobURL = URL.createObjectURL(blob);
						console.log('<a href="' + blobURL + '">' + blobURL + '</a>');
						if(blobURL !=""){
						//mediaRecorder.save(blobURL, 'blob.wav');
						console.log("calling mediaRecorder for "+ $scope.translateFromLanguage.id);
						$scope.translateMessage(blob, username, $scope.translateFromLanguage.id, $scope.translateToLanguage.id, 'blob.wav')
							.then(function(message){
								$scope.$apply(function(){
								console.log('Text =' + message);
								if(message.text)
								{	
									console.log("Got message in translate",message);
									$scope.translateMessages.push(message);
									$scope.notifyPeer(message, 'translate');
									// $scope.getTextToSpeech(message.text, 'hi-IN', 'hi-IN, Kalpana', 'Female');
								}
								});
							})
							.catch(function(error){
								console.log("An error occurred in app.js :" + error);
							})
						}
					};
					mediaRecorder.start(5000);
					// setTimeout(function() {
					// 	mediaRecorder.stop();
					// }, 5000);
				}	
		};

		$scope.roomid=$routeParams.id
		$scope.roomLink = window.location.href;

		$scope.createRoom=function(){
			var rooms = []

			$.ajax({
			async: false,
			type: 'GET',
			url: 'https://service.xirsys.com/room',
			data:{
				"ident": "sumeetha",
				"secret": "61669b9e-0659-11e8-b8e3-953124e4501c",
				//"05fe8b30-5287-11e7-bedd-7b534bcfe6c6",
				"domain":"www.imirtc.com",
				"application":"imivideochat"
				},
			success:
				function(data,status)
				{
					console.log("Data: " + data.d + "\nStatus: " + status);
					rooms = data.d;
				},
			error:
				function(data,status)
				{
					console.log("An error occurred: " + data.d + "\nStatus: " + status);
					rooms = data.d;
				},

			});
			var num= $.inArray($scope.roomid, rooms);
			if( num == -1)
			{
				$.ajax(
				{
					async: false,
					type: 'POST',
					url:'https://service.xirsys.com/room',
					data: {
					"ident": "sumeetha",
					"secret": "61669b9e-0659-11e8-b8e3-953124e4501c",
					//"05fe8b30-5287-11e7-bedd-7b534bcfe6c6",
					"domain":"www.imirtc.com",
					"application":"imivideochat",
					"room":$scope.roomid
					},
					success:
					function(data,status)
					{
						console.log("Created new room :Status: " + status);
					},
					error:
					function(data,status)
					{
						console.log("Status: " + status);
					}
				});
			}
		};

		// Toggles the state of audio
		$scope.toggleAudio = function (stream) { // stream is your local WebRTC stream
			console.log("INSIDE TOGGLEAUDIO");
			var audioTracks = stream.getAudioTracks();
			for (var i = 0; i < audioTracks.length; i++) {
				audioTracks[i].enabled = !audioTracks[i].enabled; //disabled the audiotrack
				// $scope.showToggleAudioBtn = !($scope.showToggleAudioBtn);
				console.log("AUDIO TRACK STATE :" + audioTracks[i].enabled);
			}
		};

		//Hides Video
		$scope.toggleVideo = function (stream) { // stream is your local WebRTC stream
			console.log("INSIDE TOGGLEVIDEO");
			var videoTracks = stream.getVideoTracks();
			for (var i = 0; i < videoTracks.length; i++) {
				videoTracks[i].enabled = !videoTracks[i].enabled;
				console.log("VIDEO TRACK STATE :" + videoTracks[i].enabled);
			}
		};


		// //permanently mutes audio
		// $scope.muteAudio = function (stream) { // stream is your local WebRTC stream
		// 	console.log("INSIDE MUTEAUDIO");
		// 	var audioTracks = stream.getAudioTracks();
		// 	for (var i = 0, l = audioTracks.length; i < l; i++) {
		// 		audioTracks[i].enabled = false;
		// 		//$scope.showToggleAudioBtn = !($scope.showToggleAudioBtn);
		// 		console.log("AUDIO TRACK STATE :" + audioTracks[i].enabled);
		// 	}
		// };

		// Toggle Audio for local stream
		$scope.toggleLocalAudio = function(){
			console.log("INSIDE TOGGLE LOCAL AUDIO");
			var stream;				
				try{
					//stream = remoteVideoEl.srcObject;
					if(localVideoEl.srcObject){
						stream = localVideoEl.srcObject;
					}
					else if(localVideoEl.mozSrcObject){
						stream = localVideoEl.mozSrcObject;
					}
					else if(localVideoEl.src){
						stream = localVideoEl.src;
					}
				}
				catch(err)
				{
					console.log('An error occurred with capturing stream');
				}
				if(stream){
					$scope.toggleAudio(stream);
				}
				else{
					console.log("Failed to capture local audio stream");
				}
		};

		// Toggle Video for local stream
		$scope.toggleLocalVideo = function(){
			console.log("INSIDE TOGGLE LOCAL VIDEO");
			var stream;				
				try{
					//stream = remoteVideoEl.srcObject;
					if(localVideoEl.srcObject){
						stream = localVideoEl.srcObject;
					}
					else if(localVideoEl.mozSrcObject){
						stream = localVideoEl.mozSrcObject;
					}
					else if(localVideoEl.src){
						stream = localVideoEl.src;
					}
				}
				catch(err)
				{
					console.log('An error occurred with capturing stream');
				}
				if(stream){
					$scope.toggleVideo(stream);
				}
				else{
					console.log("Failed to capture local audio stream");
				}
		};

		$scope.dataURItoBlob = function(dataURI) {
			// convert base64/URLEncoded data component to raw binary data held in a string
			var byteString;
			if (dataURI.split(',')[0].indexOf('base64') >= 0)
				byteString = atob(dataURI.split(',')[1]);
			else
				byteString = unescape(dataURI.split(',')[1]);

			// separate out the mime component
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

			// write the bytes of the string to a typed array
			var ia = new Uint8Array(byteString.length);
			for (var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}

			return new Blob([ia], {type:mimeString});
		};

		var makeOpaque = function(className){
			var videoClass = document.getElementsByClassName(className);
			videoClass[0].style.opacity =1;
		}

		var makeTransparent = function(className){
			var videoClass = document.getElementsByClassName(className);
			videoClass[0].style.opacity = 0.7;
		}

		$scope.showDataOnCanvas = function() {
			//makeOpaque($scope.localVideoClass);		
			$scope.localCanvasTimer = window.setInterval(function() {
				//var width = 300;
				//var height = video.videoHeight / (video.videoWidth/width);
				bgContext.drawImage(video, 0,0,bgCanvas.width,bgCanvas.height) //TODO: fix width and height
			}, 10);
		};

		var removeRoomLink = function(){
			var link = document.getElementById('room-link-overlay');
			link.style.display = "none";
		}

		var addRoomLink = function(){
			var link = document.getElementById('room-link-overlay');
			link.style.display = "block";
		}

		$scope.showDataOnRemoteCanvas = function() {
			//console.log("Remote stream :"+ remoteStream);
			//makeOpaque($scope.remoteVideoClass);
			removeRoomLink();
			$scope.remoteCanvasTimer = window.setInterval(function() {
				//var width = 300;
				//var height = video.videoHeight / (video.videoWidth/width);
				//console.log("Inside showDataOnRemoteCanvas");
				bgRemoteContext.drawImage(remoteVideo, 0,0,bgRemoteCanvas.width,bgRemoteCanvas.height) //TODO: fix width and height
			}, 10);
			
		};

		$scope.sortEmotions = function(emotions) {
			// Create items array
			var items = Object.keys(emotions).map(function(key) {
				return [key, emotions[key]];
			});

			// Sort the array based on the second element
			items.sort(function(first, second) {
				return second[1] - first[1];
			});

			return items.slice(0,1);
			// console.log(items.slice(0, 1));
		};

		// $scope.getEmotionValues = function(emotions){
		//     var emotionValues = [];
		//     var num = 0;
		// 	var length = Object.keys(emotions).length;
		//     _.each(emotions, function(value, key) {
		//         if($scope.emotionValues.length <= length){
		//             $scope.emotionValues.push({'name':key, 'data':[value]});
		//         }
		//         else{
		//             $scope.emotionValues[num]['data'].push(value);
		//         }
		//         num++;
		//     });
		// };

		$scope.getEmotionValues = function(emotions){
			var emotionValues = [];
			var num = 0;
			var length = Object.keys(emotions).length;
			_.each(emotions, function(value, key) {
				if($scope.emotionValues.length <= length){
					$scope.emotionValues.push({'name':key, 'data':[value]});
				}
				else{
					$scope.emotionValues[num]['data'].push(value);
				}
				num++;
			});
		};

		var writeFaceDataToDiv = function(response, videoSource){
			if(videoSource == "local"){
				//$scope.localFaceData = [];
				var data = response.data;
				if(data.length != 0 && data[0]["faceAttributes"]){
					$scope.localFaceData = {"age":"","gender":"","glasses":""};
					$scope.localFaceData["age"]=data["0"]["faceAttributes"]["age"];
					$scope.localFaceData["gender"]=data["0"]["faceAttributes"]["gender"];
					$scope.localFaceData["glasses"]=data["0"]["faceAttributes"]["glasses"];
				}
				//$scope.localVisionData["color"]=data["color"]["dominantColors"];
				//$scope.localVisionData["description"]=data["description"]["captions"];
			}
			else if(videoSource == "remote"){
				var data = response.data;
				if(data.length != 0 && data[0]["faceAttributes"]){
					$scope.remoteFaceData = {"age":"","gender":"","glasses":""};
					$scope.remoteFaceData["age"]=data["0"]["faceAttributes"]["age"];
					$scope.remoteFaceData["gender"]=data["0"]["faceAttributes"]["gender"];
					$scope.remoteFaceData["glasses"]=data["0"]["faceAttributes"]["glasses"];
				}
			
			}
		}
		
		/* Sumeetha: Gets image data like adultscore, color, caption etc. */
		var writeVisionDataToDiv = function(response, videoSource){
			if(videoSource == "local"){
				$scope.localVisionData = {"adultScore":"","color":"","description":""};
				var data = response.data;
				//console.log(data);
				$scope.localVisionData["adultScore"]= data["adult"]["adultScore"] !== "undefined"?data["adult"]["adultScore"].toFixed(3):"";
				//$scope.localVisionData["racyScore"]=data["adult"]["racyScore"];
				$scope.localVisionData["color"]=data["color"]["dominantColors"] !== "undefined"?data["color"]["dominantColors"]:"";
				$scope.localVisionData["description"]=data["description"]["captions"][0]["text"] !=="undefined" ?data["description"]["captions"][0]["text"]:"";
			}
			else if(videoSource == "remote"){
				var data = response.data;
				//$scope.remoteVisionData = {};
				$scope.remoteVisionData = {"adultScore":"","color":"","description":""};
				$scope.remoteVisionData["adultScore"]= data["adult"]["adultScore"]!=="undefined"? data["adult"]["adultScore"].toFixed(3):"";
				//$scope.remoteVisionData["racyScore"]=data["adult"]["racyScore"];
				$scope.remoteVisionData["color"]=data["color"]["dominantColors"] !== "undefined" ?data["color"]["dominantColors"]:"";
				$scope.remoteVisionData["description"]=data["description"]["captions"][0]["text"] !== "undefined" ?data["description"]["captions"][0]["text"]:"";
				}
		}
			

		var getLargestIndex = function(objects){
			var largestIndex;
			if( objects.length != 0){
				largest = objects[0]["score"];
				for (var i = 0; i < objects.length; i++) {
					if (largest <= objects[i]["score"] ) {
						largest = objects[i]["score"];
						largestIndex = i;
					}
				}
			}
			return largestIndex;
		}

		/* Sumeetha: Gets object detection data and OCR */
		var writeObjectDetectionDataToDiv = function(response, videoSource){
			if(videoSource == "local"){
				$scope.localObjectDetectionData = {"objects":"","OCR":""};
				var data = response.data;
				// var largest;
				// var objects = data["LabelAnnotation"];
				// var largestIndex = getLargestIndex(objects);
				// console.log(objects);
				// if(largestIndex){
				// 	$scope.localObjectDetectionData["objects"]= objects[largestIndex]["description"];
				// }
				$scope.localObjectDetectionData["objects"] = (data["LabelAnnotation"].length==0)?"":data["LabelAnnotation"][0]["description"];
				$scope.localObjectDetectionData["OCR"]= (data["TextAnnotation"].length==0)?"":data["TextAnnotation"][0]["description"];
			}
			else if(videoSource == "remote"){
				$scope.remoteObjectDetectionData = {"objects":"","OCR":""};
				var data = response.data;
				// var largest;
				// var objects = data["LabelAnnotation"];
				// var largestIndex = getLargestIndex(objects);
				// console.log(objects);
				// if(largestIndex){
				// 	$scope.remoteObjectDetectionData["objects"]= objects[largestIndex]["description"];
				// }
				$scope.remoteObjectDetectionData["objects"] = (data["LabelAnnotation"].length==0)?"":data["LabelAnnotation"][0]["description"];
				$scope.remoteObjectDetectionData["OCR"]= (data["TextAnnotation"].length==0)?"":data["TextAnnotation"][0]["description"];
				
			}
		}

		var dateUTC = function(){
			//  var date = new Date();
			//  var dd = date.getDate();
			// var mm = date.getMonth();
			// var yyyy = date.getFullYear();

			return Math.floor((new Date()).getTime());
		};
		
		var getEmotionIndex = function(emotion){
			var index ;
			switch(emotion){
				case "anger": index =0;
						break;
				case "contempt": index =1;
						break;
				case "disgust": index =2;
						break;
				case "fear": index=3;
						break;
				case "happiness": index=4;
						break;
				case "neutral": index=5;
						break;
				case "sadness": index=6;
						break;
				case "surprise": index =7;
						break;
			}
			return index;
		}


		$scope.writeFaceDataToCanvas = function(videoSource, response){
			if(!$scope.showVisualMode){

				if(videoSource == "local"){
					localContext3.clearRect(0, 0, localCanvas3.width, localCanvas3.height);
					
					console.log("Kairos face detection data:", response);
					//$scope.myEmotions = [];
					var faces = response.data.images;
					var font_size = 24;
					if(faces.length >= 2) {
						font_size = 22;
					} else {
						font_size = 24;
					}

					for(var k=0; k<faces.length; k++) {
						var face = faces[k].transaction;
						if (face.status == "success"){
							var dx = face.topLeftX;
							var dy = face.topLeftY + face.height;
							localContext3.fillStyle = "rgba(256, 99, 71, 0.4)";
							localContext3.fillStyle = "#ff6347";
							localContext3.font = font_size + 'px helvetica';
							console.log("Face from Kairos faceRecognition api :",face.subject_id);
							localContext3.fillText(face.subject_id, dx - 30, dy-22);
							var confidence = parseInt(face.confidence * 100)+"%";
							localContext3.fillText(confidence, dx - 30, dy);
						}
						else if (face.status == "failure"){
							var dx = face.topLeftX;
							var dy = face.topLeftY + face.height;
							localContext3.fillStyle = "rgba(256, 99, 71, 0.4)";
							localContext3.fillStyle = "#ff6347";
							localContext3.font = font_size + 'px helvetica';
							console.log("Face from Kairos faceRecognition api :","Unknown");
							localContext3.fillText("Unknown", dx - 30, dy-22);
						}

					}
				}
				else if(videoSource == "remote"){
					remoteContext3.clearRect(0, 0, remoteCanvas3.width, remoteCanvas3.height);
					
					console.log("Kairos face detection data:", response);
					//$scope.myEmotions = [];
					var faces = response.data.images;
					var font_size = 12;
					if(faces.length >= 2) {
						font_size = 10;
					} else {
						font_size = 12;
					}

					for(var k=0; k<faces.length; k++) {
						var face = faces[k].transaction;
						if (face.status == "success"){
							var dx = face.topLeftX;
							var dy = face.topLeftY + face.height;
							remoteContext3.fillStyle = "rgba(256, 99, 71, 0.4)";
							remoteContext3.fillStyle = "#ff6347";
							remoteContext3.font = font_size + 'px helvetica';
							console.log("Face from Kairos faceRecognition api :",face.subject_id);
							remoteContext3.fillText(face.subject_id, dx - 30, dy-22);
							var confidence = parseInt(face.confidence * 100)+"%";
							remoteContext3.fillText(confidence, dx - 30, dy);
						}
						else if (face.status == "failure"){
							var dx = face.topLeftX;
							var dy = face.topLeftY + face.height;
							remoteContext3.fillStyle = "rgba(256, 99, 71, 0.4)";
							remoteContext3.fillStyle = "#ff6347";
							remoteContext3.font = font_size + 'px helvetica';
							console.log("Face from Kairos faceRecognition api :","Unknown");
							remoteContext3.fillText("Unknown", dx - 30, dy-22);
						}

					}
				}
				else{
					console.log("INVALID VIDEO SOURCE");
				}
			}
		};

		$scope.drawToCanvas = function(videoSource, response, multipleFaces){
			if(multipleFaces==null ||(!multipleFaces && !multipleFaces[0].identity)){
				multipleFaces = [];
			}
		//$scope.drawToCanvas = function(videoSource, response){
			console.log(videoSource, response, multipleFaces);

			if(!$scope.showVisualMode){

				if(videoSource == "local"){
					localContext1.clearRect(0, 0, localCanvas1.width, localCanvas1.height);
					
					console.log("EMOTION data:", response);
					//$scope.myEmotions = [];

					var emotions = response.data.output;
					var font_size = 24;
					if(emotions.length == 2) {
						font_size = 24;
					} else {
						font_size = 22;
					}

					for(var k=0; k<emotions.length; k++) {
						var emotion = emotions[k];
						var faceFromFaceRecognition = multipleFaces[$scope.getClosestBoundingBoxIndex(emotion.faceRectangle, multipleFaces, localCanvas1.width)];
						var localMainEmotion = $scope.sortEmotions(emotion.scores); // mainEmotion is an array ['anger', '0.76']
						var image = document.getElementById("local_" + localMainEmotion[0][0]);
						var dx = emotion.faceRectangle.left;
						var dy = emotion.faceRectangle.top + emotion.faceRectangle.height;
						var f_dy = emotion.faceRectangle.bottom
						localContext1.fillStyle = "rgba(256, 99, 71, 0.4)";
						localContext1.fillRect(dx, emotion.faceRectangle.top, emotion.faceRectangle.width, emotion.faceRectangle.height);
						localContext1.fillStyle = "#ff6347";
						localContext1.font = font_size + 'px helvetica';
						console.log("Face from faceRecognition api :",faceFromFaceRecognition);
						if(faceFromFaceRecognition){
							localContext1.fillText(faceFromFaceRecognition.identity, dx - 30, dy-22);
						}
						
						// localContext1.fillText("Testing with random text", dx - 30, dy-22);
						localContext1.drawImage(image, dx + 5, dy + 5, localCanvas1.width * 0.08  , localCanvas1.height *0.08);
						//$scope.getEmotionValues(emotion.scores);
						if(localMainEmotion){
							var confidence = parseInt(localMainEmotion[0][1]*100);
							$scope.localEmotionValues.push({pointInterval:1000*10, y:getEmotionIndex(localMainEmotion[0][0]), z:confidence, name:localMainEmotion[0][0]});
							//$scope.localEmotionValues.push({x: dateUTC(), y:getEmotionIndex(localMainEmotion[0][0]), z:confidence, name:localMainEmotion[0][0]});
							$scope.allLocalEmotionValues[k] = $scope.localEmotionValues;
							console.log("Local emotions :" + JSON.stringify($scope.allLocalEmotionValues[k]));
						}
					}
				}
				else if(videoSource == "remote"){
					remoteContext1.clearRect(0, 0, remoteCanvas1.width, remoteCanvas1.height);
					
					console.log("EMOTION data:", response);
					//$scope.myEmotions = [];

					var emotions = response.data.output;
					var font_size = 12;
					if(emotions.length == 2) {
						font_size = 10;
					} else {
						font_size = 12;
					}

					for(var k=0; k<emotions.length; k++) {
						var emotion = emotions[k];
						console.log("Multiple faces :" + multipleFaces);
						var faceFromFaceRecognition = multipleFaces[$scope.getClosestBoundingBoxIndex(emotion.faceRectangle, multipleFaces, remoteCanvas1.width)];
						$scope.remoteMainEmotion = $scope.sortEmotions(emotion.scores); // mainEmotion is an array ['anger', '0.76']
						var image = document.getElementById("remote_" + $scope.remoteMainEmotion[0][0]);
						var dx = emotion.faceRectangle.left;
						var dy = emotion.faceRectangle.top + emotion.faceRectangle.height;
						remoteContext1.fillStyle = "rgba(256, 99, 71, 0.4)";
						remoteContext1.fillRect(dx, emotion.faceRectangle.top, emotion.faceRectangle.width, emotion.faceRectangle.height);
						remoteContext1.fillStyle = "#ff6347";
						remoteContext1.font = font_size + 'px helvetica';
						console.log("Face from faceRecognition api :",faceFromFaceRecognition);
						if(faceFromFaceRecognition){
						remoteContext1.fillText(faceFromFaceRecognition.identity, dx - 30, dy-22);
						}
						remoteContext1.drawImage(image, dx + 5, dy + 5, remoteCanvas1.width * 0.08  , remoteCanvas1.height *0.07);
						$scope.remoteEmotionValues.push({x: new Date(), y:$scope.remoteMainEmotion[0][0], z:$scope.remoteMainEmotion[0][1]});
						$scope.allRemoteEmotionValues[k] = $scope.remoteEmotionValues;
					}
				}
				else{
					console.log("INVALID VIDEO SOURCE");
				}
			}
		};

		$scope.getFaceRecognitionFromKairos = function(url, videoSource){
			console.log(url);
			$http({
				url: '/api/facerecognitionkairos',
				method: 'POST',
				data: {'url': url},
				headers: {
					'content-type': 'application/json'
				}
			})
			.then(function(response) {
				console.log("Face Recognition data: ", response);
				$scope.writeFaceDataToCanvas(videoSource, response);
			})
			.catch(function(error) {
				console.log("Error in Recognition", error);
			});
		}

		$scope.getEmotions = function(url, videoSource) {
			var emotions = null;
			$http({
				url: '/api/emotion/image',
				method: "POST",
				data: { 'url' : url, 'library': 'azure' },
				headers: {
					"content-type": "application/json"
				}
			})
			.then(function(response) {
				emotions = response;
				console.log("Response from emotions api: "+ response.data.output);
				$scope.drawToCanvas(videoSource, emotions, null);
				//return $scope.getFaceRecognition(url, videoSource)
			})
			// .then(function(response) {
			// 	console.log("Response from face recognition: "+ response.data);
			// 	if(response!= null && response.data != null && response.data.length != 0){
			// 		$scope.drawToCanvas(videoSource, emotions, response.data);
			// 	}
			// 	else{
			// 		$scope.drawToCanvas(videoSource, emotions, null);
			// 	}
			// })
			.catch(function(error) {
				console.log("Error in emotions:", error);
			});
		};

		$scope.queueRunnerAudio = function() {
			// This will be called when a textToSpeech success callback is recieved.
			// This will have the algo to play audios depending on the queue state and current order.
			// This will also play sounds one after the other using sound callbacks.
			

			for (item in $scope.textToSpeechQueue) {
				if($scope.textToSpeechQueue[item].url != '' && $scope.textToSpeechQueue[item].order <= $scope.currentTextToSpeechOrder) {
					$scope.sounds_to_play.push($scope.textToSpeechQueue[item].url);
					delete $scope.textToSpeechQueue[item]
					$scope.currentTextToSpeechOrder += 1;
				}
			}

			console.log($scope.sounds_to_play);

			// $scope.playAudio();
		};

		$scope.playAudio = function() {
			// console.log($scope.sounds_to_play, "inside playAudio");
			if($scope.sounds_to_play.length > 0) {
				clearInterval($scope.audioInterval);
				var sound = new Howl({
				src: [$scope.sounds_to_play[0]]
				});
				sound.on('end', function(){
					$scope.sounds_to_play.splice(0,1);
					if($scope.sounds_to_play.length > 0) {
						$scope.playAudio();
					} else {
						$scope.audioInterval = window.setInterval(function() {
							$scope.playAudio();
						}, 1000);
					}
				});
				sound.play();
			}
		};

		$scope.getTextToSpeech = function(text, lang, voice, gender) {
			var filename = "your_file_name_0_" + (new Date()).getTime();
			$scope.textToSpeechQueue[filename] = {
				order: $scope.textToSpeechCount + 1,
				url: ''
			};
			$scope.textToSpeechCount += 1;
			$http({
				url: 'https://35.167.96.110/texttospeech',
				method: "POST",
				data: { 
					"bot_id": "1289ijims11omqs",
					"user_id": "kjnjkn1n9s101s",
					"filename": "your_file_name_0_" + (new Date()).getTime(),
					"library": "azure",
					"text": text,
					"gender": gender,
					"voice_ms": voice,
					"lang": lang
				},
				headers: {
					"content-type": "application/json"
				}
			})
			.then(function(response) {
				console.log(response);
				if(response.data.audio_url) {
					var url = response.data.audio_url;
					var returnedFileNameSplits = url.split('/');
					var returnedFileNameWithExtension = returnedFileNameSplits[returnedFileNameSplits.length-1];
					var returnedFileName = returnedFileNameWithExtension.split('.')[0];
					console.log(returnedFileName);
					console.dir($scope.textToSpeechQueue);
					console.log($scope.textToSpeechQueue, $scope.textToSpeechQueue[returnedFileName]);
					$scope.textToSpeechQueue[returnedFileName].url = response.data.audio_url;
					console.log($scope.textToSpeechQueue);
					$scope.queueRunnerAudio();
				}
				//$scope.drawToCanvas(videoSource, response);
			})
			.catch(function(error) {
				console.log("Error in Text to Speech:", error);
			});
		};

		$scope.getCoordinatesFromFaceRecognitionBox = function(face, width) {
			var top = face.top;
			var bottom = face.bottom;
			var left = width - face.right;
			var right = width - face.left;

			return  {top: top, left: left, width: right-left, height: bottom-top};
		};

		$scope.getClosestBoundingBoxIndex = function(singleFace, multipleFaces, width) {
			// Single face comes from emotion api.
			// Multiple faces come from face recognition api.
			var all_diff = [];
			for(face in multipleFaces) {
				coordFace = $scope.getCoordinatesFromFaceRecognitionBox(multipleFaces[face], width);
				var diff = 0;
				diff = diff + Math.abs(coordFace.top - singleFace.top);
				diff = diff + Math.abs(coordFace.left - singleFace.left);
				diff = diff + Math.abs(coordFace.width - singleFace.width);
				diff = diff + Math.abs(coordFace.height - singleFace.height);
				all_diff.push(diff);
			}

			var lowest_diff_index = all_diff.indexOf(Math.min.apply(null, all_diff));
			return lowest_diff_index;
		};

		$scope.getFaceRecognition = function(url, videoSource) {
			console.log(url);
			var deferred = $q.defer();

			$http({
				url: '/api/facerecognition',
				method: 'POST',
				data: {'url': url},
				headers: {
					'content-type': 'application/json'
				}
			})
			.then(function(response) {
				console.log("Face Recognition data: ", response);
				deferred.resolve(response);
				/*
					response.data contains:
					- [
						{
							"identity": "Aditya",
							"bottom": 260,
							"left": 53,
							"right": 239,
							"top": 74
						},
						{
							"identity": "Prabhjot",
							"bottom": 260,
							"left": 53,
							"right": 239,
							"top": 74
						}
					]
				*/
				// $scope.drawToCanvas(videoSource, response);
			})
			.catch(function(error) {
				console.log("Error in Recognition", error);
				deferred.reject(error);
			});

			return deferred.promise;
		};

		/* Sumeetha: Call MS Azure vision API to get image tags */
		$scope.getVisionData = function(url, videoSource) {
			console.log(url);
			$http({
				url: '/api/vision',
				method: "POST",
				data: { 'url' : url, 'library': 'azure' },
				headers: {
					"content-type": "application/json"
				}
			})
			.then(function(response) {
				console.log("Vision data:", response);
				writeVisionDataToDiv(response, videoSource);
				// return response;
			})
			.catch(function(error) {
				console.log("Error in vision:", error);
			});
		};

		/* Sumeetha: Call Google vision API to detect objects, OCR etc. */
		$scope.getObjectDetectionData = function(url, videoSource) {
			console.log(url);
			$http({
				url: '/api/objectdetection',
				method: "POST",
				data: { 'url' : url, 'library': 'google' },
				headers: {
					"content-type": "application/json"
				}
			})
			.then(function(response) {
				console.log("Object detection data:", response);
				writeObjectDetectionDataToDiv(response, videoSource);
				// return response;
			})
			.catch(function(error) {
				console.log("Error in object detection:", error);
			});
		};


		/* Sumeetha: Call MS Azure face api to capture face attributes */
		$scope.getFaceData = function(url, videoSource) {
			$http({
				url: '/api/face/image',
				method: "POST",
				data: { 'url' : url, 'library': 'azure' },
				headers: {
					"content-type": "application/json"
				}
			})
			.then(function(response) {
				console.log("Face data:", response);
				writeFaceDataToDiv(response, videoSource);
				// return response;
			})
			.catch(function(error) {
				console.log("Error in faces:", error);
			});
		};

		$scope.getLocalImageData = function() {
			$scope.captureStream("localVideoEl");
			if(localStream){
				console.log("Local Image Data");
				var dataURL = bgCanvas.toDataURL('image/jpeg', 0.8);
				var blob = $scope.dataURItoBlob(dataURL);
				var fd = new FormData();
				var emotions;
				var visionData;
				var faceData;
				fd.append("image", blob, "local_img_"  + (new Date()).getTime());

				$.ajax({
					type: 'POST',
					url: '/api/uploadImageToS3',
					data: fd,
					processData: false,
					contentType: false
				}).done(function(data) {
					console.log("LOCAL IMAGE SAVED: ", data);
					var image_url = data.url;
					//$scope.getFaceRecognition(image_url, "local");//comment this to remove face recognition
					// REPLACE BY SERVER CALLS
					
					$scope.getEmotions(image_url, "local");
					$scope.getFaceRecognitionFromKairos(image_url, "local");
					$scope.getVisionData(image_url, "local");
					$scope.getFaceData(image_url, "local");
					$scope.getObjectDetectionData(image_url, "local");
					
				})
			}	
		};

		$scope.getRemoteImageData = function() {
			$scope.captureStream("remoteVideoEl");
			if(remoteStream){
				var dataURL = bgRemoteCanvas.toDataURL('image/jpeg', 0.8);
				var blob = $scope.dataURItoBlob(dataURL);
				var fd = new FormData();
				fd.append("image", blob, "remote_img_"  + (new Date()).getTime());

				$.ajax({
					type: 'POST',
					url: '/api/uploadImageToS3',
					data: fd,
					processData: false,
					contentType: false
				}).done(function(data) {
					console.log("REMOTE IMAGE SAVED: ", data);
					var image_url = data.url;
					//Uncommented face recognition code
					//$scope.getFaceRecognition(image_url, "remote");
					// REPLACE BY SERVER CALLS.
					$scope.getEmotions(image_url, "remote");
					$scope.getFaceRecognitionFromKairos(image_url, "remote");
					$scope.getVisionData(image_url, "remote");
					$scope.getFaceData(image_url, "remote");
					$scope.getObjectDetectionData(image_url, "remote");
				})
			}
		};
		
		function positionLoop(ctracker) {
			//requestAnimationFrame(positionLoop(ctracker));
			var positions = ctracker.getCurrentPosition();
			// positions = [[x_0, y_0], [x_1,y_1], ... ]
			// do something with the positions ...
		}

		// function drawLoop(overlay, overlayCC, ctrack) {
		// 	//requestAnimationFrame(drawLoop(canvasInput, cc, ctracker));
		// 	// cc.clearRect(0, 0, canvasInput.width, canvasInput.height);
		// 	// ctracker.draw(canvasInput);
		// 	requestAnimeFrame(drawLoop(overlay, overlayCC, ctrack));
		// 	overlayCC.clearRect(0, 0, vid_width, vid_height);
		// 	if (ctrack.getCurrentPosition()) {
		// 		ctrack.draw(overlay);
		// 	}
		//   }
		
		function drawLocalLoop() {
			if(!$scope.stopCV){  
				requestAnimFrame(drawLocalLoop);
				// localContext.translate(localCanvas.width, 0);
				// localContext.scale(-1, 1);
				localContext2.clearRect(0, 0, localCanvas2.width, localCanvas2.height);
				if (ctrackerLocal.getCurrentPosition()) {
					ctrackerLocal.draw(localCanvas2);
				}
			}
		}
		
		function drawRemoteLoop() {
			if(!$scope.stopCV){  
				requestAnimFrame(drawRemoteLoop);
				remoteContext2.clearRect(0, 0, remoteCanvas2.width, remoteCanvas2.height);
				if (ctrackerRemote.getCurrentPosition()) {
					ctrackerRemote.draw(remoteCanvas2);
				}
			}
		}

		$scope.runCvLocally = function() {
			// $scope.captureStream("localVideoEl");
			// $scope.captureStream("remoteVideoEl");
			if(localStream)
			{
				console.log("Inside runCVLocally");
				localVideoEl.play();
				ctrackerLocal.start(localVideoEl);
				drawLocalLoop();
			}	
		};

		$scope.runCvLocallyForRemoteVideo = function(){
			if(remoteStream)
				{
					console.log("Inside runCvLocallyForRemoteVideo");
					remoteVideoEl.play();
					ctrackerRemote.start(remoteVideoEl);
					drawRemoteLoop();
				}
		}

		$scope.toggleVisualMode = function(){
			console.log($scope.showVisualMode);
			if($scope.showVisualMode){
				$scope.startVisualMode();
			}
			else{
				$scope.stopVisualMode();
			}
		}

		$scope.startVisualMode = function(){

			$scope.stopCV = false;
			//using ctracker CV library to run face recognition locally
			ctrackerLocal = new clm.tracker({useWebGL : true});
			ctrackerLocal.init();
			ctrackerRemote = new clm.tracker({useWebGL : true});
			ctrackerRemote.init();
			console.log("Entered Visual Mode");
			$scope.showVisualMode = false;
			$scope.captureStream("localVideoEl");
			$scope.captureStream("remoteVideoEl");

			//run face tracking locally using clmtrackr library
			//$scope.runCvLocally();
			//$scope.runCvLocallyForRemoteVideo();

			//run emotion detection, face recognition etc using APIs
			$scope.timer = setInterval(function() {
				$scope.getLocalImageData();
				$scope.getRemoteImageData();
				if(document.getElementById('emotion-container')){
					$scope.alertWhenEmotionTabActive();
				}
			}, 3000);
		};

		$scope.stopVisualMode = function() {
			console.log("Stopped Visual Mode");
			$scope.showVisualMode = true;
			localContext1.clearRect(0, 0, localCanvas1.width, localCanvas1.height);
			localContext2.clearRect(0, 0, localCanvas2.width, localCanvas2.height);
			localContext3.clearRect(0, 0, localCanvas3.width, localCanvas3.height);
			remoteContext1.clearRect(0, 0, remoteCanvas1.width, remoteCanvas1.height);
			remoteContext2.clearRect(0, 0, remoteCanvas2.width, remoteCanvas2.height);
			remoteContext3.clearRect(0, 0, remoteCanvas3.width, remoteCanvas3.height);
			clearInterval($scope.timer);
			ctrackerLocal.stop();
			ctrackerRemote.stop();
			$scope.stopCV = true;
		};


		// Send a message to one or all peers.
		$scope.notifyPeer = function (message, messageType) {
			//$event.preventDefault();
			if (!p.signal) {
				console.log('You are not yet connected to the signalling server');
				addMessage('You are not yet connected to the signalling server');
				return;
			}
			var peer = selectedPeer("message");
			if (!!peer) {
				p.signal.send(messageType, message, peer);
			} else {
				p.signal.send(messageType, message);
			}
			//$scope.addTranscriptMessage((!!peer) ? 'To ' + peer : 'To all peers', message);
			//messageEl.value = '';
		};

		// Add a message to the conversation.
		$scope.addTranscriptMessage = function ($msgTrail) {
			$scope.$apply(function(){
				$scope.transcriptMessages.push($msgTrail);
			})
		};

		// Add a message to the conversation.
		$scope.addTranslateMessage = function ($msgTrail) {
			$scope.$apply(function(){
				$scope.translateMessages.push($msgTrail);
			})
		};

		
	// Getting references to page DOM for signalling.
			
		/* User interface handler functions */
		
		// When the connect button is clicked hide log-in, check the user-
		// name is valid, cancel automatic answers (see xirsys.p2p.js
		// onSignalMessage method) and open a connection to the server.
		loginEl.onsubmit = function ($event) {
			$event.preventDefault();
			$scope.username = usernameEl.value.replace(/\W+/g, '');
			username = usernameEl.value.replace(/\W+/g, '');
			if (!username || username == '') {
				return;
			}
			$scope.loggedIn = true;
			$('#login-guest-modal').modal('hide');
			// loginEl.parentNode.style.visibility = 'hidden';
			// logOutEl.style.visibility = 'visible';
			$scope.createRoom();
			var connectionProperties = xirsysConnect.data;
			connectionProperties.room=$scope.roomid;
			connectionProperties.username = username;
			connectionProperties.automaticAnswer = automaticAnswer;
			p.open(connectionProperties);
			//$scope.captureStream("localVideoEl");
			$scope.showDataOnCanvas();
			console.log('Logged into room :' + connectionProperties.room);
			// $scope.audioInterval = window.setInterval(function() {
			// 	$scope.playAudio();
			// }, 1000);
		};
		
		// Log out and reset the interface.
		logOutEl.onclick = function ($event) {
			$event.preventDefault(); // prevents redirecting of link
			$scope.username = '';
			username = '';
			$scope.loggedIn = false;
			// while (usernameLabelEl.hasChildNodes()) {
			// 	usernameLabelEl.removeChild(usernameLabelEl.lastChild);
			// }
			// usernameLabelEl.appendChild(document.createTextNode('[Not logged in]'));
			// login.parentNode.style.visibility = 'visible';
			// logOutEl.style.visibility = 'hidden';
			removeAllPeers();
			p.hangUp();
			detachMediaStream(localVideoEl);
			p.close();
			$timeout(function(){
				window.location.href = "#";
			},100);
		}

		var peerName;

		// Peer button function
		$('#peers').delegate('button', 'click', function() {

			//$scope.peerName = $(this).attr('value');
			peerName = $(this).attr('value');
			//$(this).closest('button').prop('checked', true);

			// peerName = $(this).attr('value');
			if(peerName != $scope.peerName){
				$(this).closest('button').prop('checked', true);
				$scope.$apply(function(){
					$($scope.peerName).prop('checked', false);
					$scope.peerName = peerName;
				});
			}

			if($(this).hasClass('call-peer')){
				makingCall = true;
				$scope.messageToCaller = "Calling " + peerName;
				console.log($scope.messageToCaller);
				$('#call-outgoing-modal').modal('show');
				$scope.callPeer(peerName);
			}
			else if($(this).hasClass('message-peer')){
				console.log("Messaging", peerName);
				console.log("Sending message to ", peerName);
				// $scope.$apply(function(){
				// 	$scope.messagePeer(peerName);
				// });
				$('#message-popup').addClass('message-popup-open');
				$('#message-bubble-button').addClass('message-bubble-out');
			}
			// do something with the text
		});
		
		// // Send a message to one or all peers.
		// sendMessageEl.onsubmit = function ($event) {
		// 	$event.preventDefault();
		// 	if (!p.signal) {
		// 		addMessage('You are not yet connected to the signalling server');
		// 		return;
		// 	}
		// 	var peer = selectedPeer();
		// 	if (!!peer) {
		// 		p.signal.send('message', messageEl.value, peer);
		// 	} else {
		// 		p.signal.send('message', messageEl.value);
		// 	}
		// 	sendMessage((!!peer) ? 'To ' + peer : 'To all peers', messageEl.value);
		// 	messageEl.value = '';
		// };

		// Send a message to one or all peers.
		$scope.messagePeer = function(peerName){
			if (!p.signal) {
				addMessage('You are not yet connected to the signalling server');
				return;
			}
			//var peer = selectedPeer("message");
			var peer = (peerName == "__all__") ? undefined : peerName; 
			console.log("Messaging to peer" , peer);
			if (!!peer) {
				p.signal.send('message', messageEl.value, peer);
			} else {
				p.signal.send('message', messageEl.value);
			}
			sendMessage((!!peer) ? 'To ' + peer : 'To all peers', messageEl.value);
			messageEl.value = '';
		};
		
		// Initiates a call, if a single peer has been selected.
		// callPeerEl.onclick = function () {
		// 	var peerName = selectedPeer();
		// 	console.log(peerName);
		// 	if (!!peerName) {
		// 		$scope.peerNotSelected = false;
		// 		$scope.peerSelected = true;
		// 		$scope.$apply();
		// 		p.call(peerName);
		// 		// $timeout(function(){
		// 		// 	p.call(peerName);
		//     	// }, 1000);
		// 		$('#call-outgoing-modal').modal('toggle');
		// 		$scope.messageToCaller = "Calling " + peerName;
		// 		$scope.$apply();
		// 		//addMessage('Calling ' + peerName);
		// 		console.log("Calling showDataOnRemoteCanvas");
		// 		$scope.showDataOnRemoteCanvas();
		// 		// N.B. This demo doesn't include a method for noting
		// 		// rejected calls. This could be added in the demo by
		// 		// sending a message when rejecting the call, but it would
		// 		// be preferable to extend the xirsys.p2p class to
		// 		// automatically emit an event to the same effect.
		// 	} else {
		// 		//addMessage('Error', 'You must select a single peer before initiating a call');
		// 		$scope.peerNotSelected = true;
		// 		$scope.peerSelected = false;
		// 		console.log($scope.peerSelected + " " +$scope.peerNotSelected);
		// 		$('#call-outgoing-modal').modal('toggle');
		// 	}
		// };

		$scope.callPeer = function(peerName){
			if (!!peerName) {
				// console.log(remoteStream);
				// console.log(localStream);
				// $timeout(function(){
				// 	$scope.captureStream('localVideoEl')
				// },10);
				callAudio.play();
				p.call(peerName);
				console.log('Calling ' + peerName);
				console.log("Calling showDataOnRemoteCanvas");
				$scope.showDataOnRemoteCanvas();


				// N.B. This demo doesn't include a method for noting
				// rejected calls. This could be added in the demo by
				// sending a message when rejecting the call, but it would
				// be preferable to extend the xirsys.p2p class to
				// automatically emit an event to the same effect.
			} else {
				makingCall = false;
				//addMessage('Error', 'You must select a single peer before initiating a call');
			}
		}

		$scope.cancelCall = function(peerName){
			callAudio.pause();
			var message = "cancel";
			$scope.notifyPeer(message, 'callConnected');
		}
		
		// Ends current call, if any.
		hangUpEl.onclick = function () {
			//remoteStream = null;
			var message="hanging-up";
			p.hangUp();	

			$scope.notifyPeer(message, 'callHangup');
			//p.close();
			//removeAllPeers();
			detachMediaStream(remoteVideoEl);
			// remoteVideoEl.srcObject = null;
			// remoteVideoEl.src = null;
			remoteContext1.clearRect(0, 0, remoteCanvas1.width, remoteCanvas1.height);
			remoteContext2.clearRect(0, 0, remoteCanvas2.width, remoteCanvas2.height);
			bgRemoteContext.clearRect(0, 0, bgRemoteCanvas.width, bgRemoteCanvas.height);
			clearInterval($scope.remoteCanvasTimer);
			makeTransparent($scope.remoteVideoClass);
			addRoomLink();
			// $scope.captureStream('localVideoEl');
			// $scope.captureStream('remoteVideoEl');
			
			////TODO : Add a logout button and move this code to logout

			// localStream = null;
			// localContext1.clearRect(0, 0, localCanvas1.width, localCanvas1.height);
			// localContext2.clearRect(0, 0, localCanvas2.width, localCanvas2.height);
			// bgContext.clearRect(0, 0, bgCanvas.width, bgCanvas.height);
			// clearInterval($scope.localCanvasTimer);
			//detachMediaStream(localVideoEl);
			//removeAllPeers();
			//makeTransparent($scope.localVideoClass);
			//// usernameLabelEl.removeChild(usernameLabelEl.lastChild);
			// $scope.username=null;
			// username = null;
			//p.close();		
		}
		
		// Requesting full screen.============================================================================================================
		localFullScreenEl.onclick = function ($evt) {
			fullScreenVideo(bgCanvas);
			fullScreenVideo(localCanvas1);
			fullScreenVideo(localCanvas2);
		};

		remoteFullScreenEl.onclick = function ($evt) {
			fullScreenVideo(bgRemoteCanvas);
			fullScreenVideo(remoteCanvas1);
			fullScreenVideo(remoteCanvas2);
		};
		
		/* Other interface functions */
		
		// When a peer connects check to see if it is the user. If it is 
		// update the user's label element. If it is not check if the peer
		// is already listed and add an element if not.s
		
		var addPeer = function ($peerName) {
			if ($peerName == username) {
				// while (usernameLabelEl.hasChildNodes()) {
				// 	usernameLabelEl.removeChild(usernameLabelEl.lastChild);
				// }
				// usernameLabelEl.appendChild(document.createTextNode(stripLeaf($peerName)));
			} else {
				if (!document.getElementById('peer-' + $peerName)) {
					var nodeEl = document.createElement('div'), // create
						callBtnEl = document.createElement('button');
						messageBtnEl = document.createElement('button');
						textEl = document.createElement('p');
						callIconEl = document.createElement('i');
						messageIconEl = document.createElement('i');
					// Display peer name
					textEl.className = 'pull-left';
					textEl.setAttribute('value', $peerName);
					// Peer Call button 
					callBtnEl.setAttribute('value', $peerName);
					$(callBtnEl).addClass('btn btn-success btn-sm pull-right call-peer');
					callBtnEl.setAttribute('name', 'peer');
					$(callIconEl).addClass('fa fa-phone');
					// Peer Message Button
					messageBtnEl.setAttribute('value', $peerName);
					$(messageBtnEl).addClass('btn btn-primary btn-sm pull-right message-peer')
					messageBtnEl.setAttribute('name', 'peer');
					$(messageIconEl).addClass('fa fa-envelope');
					callBtnEl.appendChild(callIconEl);
					messageBtnEl.appendChild(messageIconEl);
					nodeEl.appendChild(callBtnEl);
					nodeEl.appendChild(messageBtnEl);
					nodeEl.appendChild(textEl);
					nodeEl.appendChild(document.createTextNode(stripLeaf($peerName)));
					nodeEl.id = 'peer-' + $peerName;
					nodeEl.className = 'peer';
					peersEl.appendChild(nodeEl);
					$scope.$apply(function(){
						$scope.newPeerName = $peerName;
					});
					console.log("Added peer "+ $scope.newPeerName);
					showPeerConnectedNotification();
				}
			}
		};
		
		// Removes peer elements from the page when a peer leaves.
		var removePeer = function ($peerName) {
			try{
				if($peerName!= null && $peerName!= 'undefined'){
					var peersEl = document.getElementById('peers');
					console.log("Removing peer ",$peerName);
						var nodeEl = document.getElementById('peer-' + $peerName);
						console.log(nodeEl);
						if(nodeEl){
							peersEl.removeChild(nodeEl);
						}
				}
			}
			catch(e){
					console.log("An error occurred while disconnecting from peer "+ e);
			}
		};
		
		// For resetting the peers list, leaving the __all__ selector only.
		var removeAllPeers = function () {
			var peersEl = document.getElementById('peers');
			var selectors = peersEl.getElementsByTagName('div'),
				peerSelectors = [];
			for (var i = 0; i < selectors.length; i++) {
				if (selectors[i].className.indexOf('peer') !== -1) {
					peerSelectors.push(selectors[i]);
				}
			}
			for (var i = 0; i < peerSelectors.length; i++) {
				peersEl.removeChild(peerSelectors[i]);
			}
		};
		
		// Get the name of the peer the user has selected.
		var selectedPeer = function (type) {
			if(type === "message"){
				var peerEl = document.getElementsByClassName('message-peer');
				for (var i=0, l=peerEl.length; i<l; i++) {
					if (peerEl[i].checked) {
						//peerEl[i].checked = false;
						return (peerEl[i].value == '__all__') ? 
							undefined : peerEl[i].value;
						//return peerEl[i].value;
					}
				}
			}
			else if(type === "call"){
				var peerEl = document.getElementsByClassName('call-peer');
				for (var i=0, l=peerEl.length; i<l; i++) {
					if (peerEl[i].checked) {
						// return (peerEl[i].value == '__all__') ? 
						// 	undefined : peerEl[i].value;
						return peerEl[i].value;
					}
				}
			}

		};

		var sendMessage  =function($msgLeader, $msgTrail){
				var leaderid=$msgLeader.slice(0,4);
				if(leaderid === "From")
				{
					var msgEl = document.createElement('div'),
					leaderEl = document.createElement('strong');
					msgTextEl = document.createElement('p');
					leaderEl.appendChild(document.createTextNode('[' + formattedTime() + '] ' + $msgLeader));
					msgEl.appendChild(leaderEl);
					console.log("This is the message",$msgTrail);
					if (!!$msgTrail) {
						msgTextEl.appendChild(document.createTextNode($msgTrail));
						msgEl.appendChild(msgTextEl);
					}
					msgEl.className='left-bubble';
					// $(".left-bubble").addClass('row');
					$("#messages").append('<br>');
				
					messagesEl.appendChild(msgEl); 
					messagesEl.parentNode.scrollTop = messagesEl.parentNode.scrollHeight;
				}
				else{
					var msgEl = document.createElement('div'),
					leaderEl = document.createElement('strong');
					msgTextEl = document.createElement('p');
					leaderEl.appendChild(document.createTextNode('[' + formattedTime() + '] ' + $msgLeader));
					msgEl.appendChild(leaderEl);
					console.log("This is the message",$msgTrail);
					if (!!$msgTrail) {
						msgTextEl.appendChild(document.createTextNode($msgTrail));
						msgEl.appendChild(msgTextEl);
					}
					msgEl.className='right-bubble';
					// $(".right-bubble").addClass('row');
					$("#messages").append('<br>');

					messagesEl.appendChild(msgEl);
					messagesEl.parentNode.scrollTop = messagesEl.parentNode.scrollHeight;        
				}			
		};

		// Add a message to the conversation.
		var addMessage = function ($msgLeader, $msgTrail) {
			var msgEl = document.createElement('div'),
				leaderEl = document.createElement('strong');
			leaderEl.appendChild(document.createTextNode('[' + formattedTime() + '] ' + $msgLeader));
			msgEl.appendChild(leaderEl);
			if (!!$msgTrail) {
				msgEl.appendChild(document.createTextNode(': ' + $msgTrail));
			}
			messagesEl.appendChild(msgEl);
			messagesEl.parentNode.scrollTop = messagesEl.parentNode.scrollHeight;
		};

		// Returns a peer name without the room and application details.
		// This function may now be redundant as the format of messages from
		// the Xirsys server has changed.
		var stripLeaf = function ($p) {
			return $p.substr($p.lastIndexOf('/')+1)
		};
		
		// Returns neatly formatted digital clock style time.
		// As this demo doesn't store messages we are assuming dates are not
		// relevent information.
		var formattedTime = function () {
			var t = new Date();
			return ( '0' + t.getHours() ).slice( -2 ) + ':' + 
				( '0' + t.getMinutes() ).slice( -2 )
				
		};
		
		// Deal with an incoming call.
		// If you've turned off automatic responses then listen to call
		// offers and allow the user to decide whether to respond or not.
		// Else calls are automatically answered (see xirsys.p2p.js).
		$scope.callIncoming = function ($peer, $data) {
			if (automaticAnswer === false) {
				if ($scope.takeCall === true) {
					callAudio.pause();
					p.answer($peer, $data);
					//addMessage('Taking a call from ' + $peer);
					var message = "connected";
					$scope.notifyPeer(message, 'callConnected');
					console.log("Calling showDataOnRemoteCanvas");
					// $timeout(function(){
					// 	$scope.captureStream('remoteVideoEl');
					// 	$scope.captureStream('localVideoEl');
					// },10);
					$scope.showDataOnRemoteCanvas();
					$('#call-incoming-modal').modal('hide');
				} else {
					callAudio.pause();
					//addMessage('Failed to connect to ' + $peer);
					var message = "declined";
					$scope.notifyPeer(message, 'callConnected');
					$('#call-incoming-modal').modal('hide');
				}
			} else {

				//TODO: Check if this works
					callAudio.pause();
					p.answer($peer, $data);
					//addMessage('Taking a call from ' + $peer);
					var message = "connected";
					$scope.notifyPeer(message, 'callConnected');
					console.log("Calling showDataOnRemoteCanvas");
					$scope.showDataOnRemoteCanvas();
				//$('#call-incoming-modal').modal('show');
			}
		};
		
		// Full-screens any HTML5 video on the page.
		var fullScreenVideo = function ($video) {
			if ($video.requestFullscreen) {
				$video.requestFullscreen();
			} else if ($video.webkitRequestFullscreen) {
				$video.webkitRequestFullscreen();
			} else if ($video.mozRequestFullScreen) {
				$video.mozRequestFullScreen();
			} else if ($video.msRequestFullscreen) {
				$video.msRequestFullscreen();
			}
		};

		var showPeerConnectedNotification = function(){
			console.log("Show peer connected notification");
			var popupClass = document.getElementById('peer-connect-alert');
			popupClass.style.display="block";
			$timeout(function(){
				popupClass.style.display ="none";
			},5000)
		}
		
	/* Watching for and responding to XSDK events */
		
		var events = $xirsys.events.getInstance();
		console.log(events)
		
		// We get this when we login. There may be zero
		// to many peers at this time.
		events.on($xirsys.signal.peers, function ($evt, $msg) {
			for (var i = 0; i < $msg.users.length; i++) {
				console.log("signal from peer :"+$msg.users[i]);
				addPeer($msg.users[i]);
				console.log("Added peer "+ $msg.users[i]);
			}
		});
		
		// When a peer connects to signalling, we
		// get notified here.
		events.on($xirsys.signal.peerConnected, function ($evt, $msg) {
			console.log("Connection request from peer "+ $msg);
			addPeer($msg);
			console.log("Added peer "+ $msg);
			console.log($msg);
			console.log($evt);
		});
		
		// When a peer disconnects from the signal server we get notified.
		events.on($xirsys.signal.peerRemoved, function ($evt, $msg) {
			// remoteVideoEl.srcObject = null;
			// remoteVideoEl.src = null;
			remoteContext1.clearRect(0, 0, remoteCanvas1.width, remoteCanvas1.height);
			bgRemoteContext.clearRect(0, 0, bgRemoteCanvas.width, bgRemoteCanvas.height);
			clearInterval($scope.remoteCanvasTimer);
			removePeer($msg);
			makeTransparent($scope.remoteVideoClass);
			addRoomLink();
		});
		
		// When a peer sends you (or you and all other peers) a message.
		events.on($xirsys.signal.message, function ($evt, $msg) {
			if ($msg.sender != name) {
				sendMessage('From ' + stripLeaf($msg.sender), $msg.data);
				incrementMessageCount();
				messageAudio.play();
			}
		});

		// When a peer answers your call you get notified here.
		events.on($xirsys.signal.callConnected, function ($evt, $msg) {
			console.log("Got reply to call from  "+ $msg.peer);
			if($msg.data === "connected"){
				$scope.$apply(function(){
					$scope.messageToCaller = "Connected to " + $msg.peer;
				});
				//console.log("Calling showDataOnRemoteCanvas");
				// $scope.captureStream('remoteVideoEl');
				//$scope.showDataOnRemoteCanvas();
			}
			else if ($msg.data === "declined"){
				$scope.$apply(function(){
					$scope.messageToCaller = "Failed to connect to " + $msg.peer;
				});
			}
			else if ($msg.data === "cancel"){
				$scope.$apply(function(){
					$scope.messageToCaller = "Disconnecting from " + $msg.peer;
					$scope.takeCall = false;
				});
				$scope.callIncoming($scope.takingCallFrom, $scope.incomingPeerData);
			}
			$timeout(function(){
				$('#call-outgoing-modal').modal('hide');
			},1000);
		});

		// rkb - When a peer disconnects from a call / hangsUp a call.
		events.on($xirsys.signal.callHangup, function($evt, $msg){
			console.log("Call Disconnect", $msg);
			console.log("Call Disconnect", $evt);
			alert("Call was disconnected");

			detachMediaStream(remoteVideoEl);
			//remoteVideoEl.srcObject = null;
			//remoteVideoEl.src = null;
			remoteContext1.clearRect(0, 0, remoteCanvas1.width, remoteCanvas1.height);
			remoteContext2.clearRect(0, 0, remoteCanvas2.width, remoteCanvas2.height);
			bgRemoteContext.clearRect(0, 0, bgRemoteCanvas.width, bgRemoteCanvas.height);
			clearInterval($scope.remoteCanvasTimer);
			makeTransparent($scope.remoteVideoClass);
			addRoomLink();
		});

		// When a peer sends you (or you and all other peers) a transcript message.
		events.on($xirsys.signal.transcript, function ($evt, $msg) {
			//if ($msg.sender != name) {
			//	addMessage('From ' + stripLeaf($msg.sender), $msg.data);
			//}
			$scope.addTranscriptMessage($msg.data);
		});

		// When a peer sends you (or you and all other peers) a translate message.
		events.on($xirsys.signal.translate, function ($evt, $msg) {
			//if ($msg.sender != name) {
			//	addMessage('From ' + stripLeaf($msg.sender), $msg.data);
			//}
			$scope.addTranslateMessage($msg.data);
		});
		
		// When a peer offers you a connection for a video call.
		events.on($xirsys.p2p.offer, function ($evt, $peer, $data) {
			$scope.$apply(function(){
				$scope.takingCallFrom = $peer;
				$scope.incomingPeerData = $data;
			});
			console.log("Got a call offer from ",$scope.takingCallFrom, "with data ", $scope.incomingPeerData);
			if(automaticAnswer){
				callAudio.play();
				$scope.callIncoming($peer, $data);
			}
			else{
				$('#call-incoming-modal').modal('show');
				callAudio.play();
			}
			// callIncoming($peer, $data);
		});
		
		// Log errors in the terminal.
		events.on($xirsys.signal.error, function ($evt, $msg) {
			console.error('error: ', $msg);
			addMessage('Error', 'There has been an error in the server connection');
		});


});
