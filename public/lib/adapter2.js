'use strict';
var RTCPeerConnection = RTCPeerConnection || null,
	RTCIceCandidate = RTCIceCandidate || null,
	RTCSessionDescription = RTCSessionDescription || null,
	getUserMedia = getUserMedia || null,
	attachMediaStream = null,
	detachMediaStream = null,
	reattachMediaStream = null,
	webrtcDetectedBrowser = null;

	// Older browsers might not implement mediaDevices at all, so we set an empty object first
	if (navigator.mediaDevices === undefined) {
		navigator.mediaDevices = {};
	}
	else if (navigator.mediaDevices.getUserMedia === undefined) {
		navigator.mediaDevices.getUserMedia = function(constraints) {

		// First get ahold of the legacy getUserMedia, if present
		getUserMedia = (navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

		// Some browsers just don't implement it - return a rejected promise with an error
		// to keep a consistent interface
		if (!getUserMedia) {
		console.log("This is not a webrtc compatible browser");	
		//return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
		}

		// // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
		// return new Promise(function(resolve, reject) {
		// getUserMedia.call(navigator, constraints, resolve, reject);
		// });
		}
	}
	else
	{
		getUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator);
	}
	
	//getUserMedia = navigator.getUserMedia.bind(navigator)||navigator.mozGetUserMedia.bind(navigator)||navigator.webkitGetUserMedia.bind(navigator)||navigator.mediaDevices.getUserMedia.bind(navigator);
	//getUserMedia = (navigator.getUserMedia||navigator.mozGetUserMedia||navigator.webkitGetUserMedia||navigator.mediaDevices.getUserMedia).bind(navigator);

	if(!getUserMedia)
	{
		console.log("This is not a webrtc compatible browser");
	}
	else
	{
		console.log("This is a webrtc compatible browser ");
	}

	function hasRTCPeerConnection() {
		window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
		console.log("inside hasRTCPeerConnection "+ window.RTCPeerConnection);
		return !!window.RTCPeerConnection;
	}

	function hasRTCSessionDescription(){
		window.RTCSessionDescription = window.RTCSessionDescription || window.mozSessionDescription;
		console.log("inside hasRTCSessionDescription");
		return !!window.RTCSessionDescription;
	}

	function hasRTCIceCandidate(){
		window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate;
		console.log("inside hasRTCIceCandidate");
		return !!window.RTCIceCandidate;
	}

	RTCPeerConnection = hasRTCPeerConnection();
	RTCSessionDescription = hasRTCSessionDescription();
	RTCIceCandidate = hasRTCIceCandidate();


	//RTCPeerConnection = RTCPeerConnection||webkitRTCPeerConnection||mozRTCPeerConnection;
	//RTCSessionDescription = RTCSessionDescription||mozRTCSessionDescription;
	//RTCIceCandidate = RTCIceCandidate||mozRTCIceCandidate;

	attachMediaStream = function (element, stream) {
		console.log("attaching mediastream");
		if (typeof element.srcObject != 'undefined') {
			element.srcObject = stream;
		} else if (typeof element.mozSrcObject != 'undefined') {
			element.mozSrcObject = stream;
			element.play();
		} else if (typeof element.src != 'undefined') {
			element.src = URL.createObjectURL(stream);
		} else {
			console.log('Error attaching stream to element.');
		}
		console.log("attached to mediastream");
	};

	
	detachMediaStream = function (element) {
		console.log("detaching media stream");
		element.pause();
		if (typeof element.srcObject != 'undefined') {
			element.srcObject = null;
		} else if (typeof element.mozSrcObject != 'undefined') {
			element.mozSrcObject = null;
			element.pause();
			element.mozSrcObject = null;
		} else if (typeof element.src != 'undefined') {
			element.src = null;
		}
	};

	reattachMediaStream = function (to, from) {
		console.log("Reattaching media stream");
		if(from.src != 'undefined'){
			to.src = from.src;
		}
		else if(from.mozSrcObject != 'undefined'){
			to.mozSrcObject = from.mozSrcObject;
		to.play();
		}
	};	

	// The representation of tracks in a stream is changed in M26.
	// Unify them for earlier Chrome versions in the coexisting period.
	if (!webkitMediaStream.prototype.getVideoTracks) {
		webkitMediaStream.prototype.getVideoTracks = function () {
			return this.videoTracks;
		};
		webkitMediaStream.prototype.getAudioTracks = function () {
			return this.audioTracks;
		};
	}

	// New syntax of getXXXStreams method in M26.
	if (!webkitRTCPeerConnection.prototype.getLocalStreams) {
		webkitRTCPeerConnection.prototype.getLocalStreams = function () {
			return this.localStreams;
		};
		webkitRTCPeerConnection.prototype.getRemoteStreams = function () {
			return this.remoteStreams;
		};
	}

	// Fake get{Video,Audio}Tracks
	if (!MediaStream.prototype.getVideoTracks) {
		MediaStream.prototype.getVideoTracks = function () {
			return [];
		}
	}

	if (!MediaStream.prototype.getAudioTracks) {
		MediaStream.prototype.getAudioTracks = function () {
			return [];
		}
	}
