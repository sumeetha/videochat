'use strict';
var RTCPeerConnection = RTCPeerConnection || null,
	RTCIceCandidate = RTCIceCandidate || null,
	RTCSessionDescription = RTCSessionDescription || null,
	getUserMedia = getUserMedia || null,
	attachMediaStream = null,
	detachMediaStream = null,
	reattachMediaStream = null,
	webrtcDetectedBrowser = null;

var detachVideo = null;
var detachAudio = null;

	// Older browsers might not implement mediaDevices at all, so we set an empty object first
	if (navigator.mediaDevices === undefined) {
		navigator.mediaDevices = {};
		console.log("navigator.mediaDevices is undefined");
	}
	else if (navigator.mediaDevices.getUserMedia === undefined) {
		console.log("navigator.mediaDevices.getUserMedia is undefined");
		navigator.mediaDevices.getUserMedia = function(constraints) {

		// First get ahold of the legacy getUserMedia, if present
		getUserMedia = (navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

		// Some browsers just don't implement it - return a rejected promise with an error
		// to keep a consistent interface
		if (!getUserMedia) {
		console.log("This is not a webrtc compatible browser");	
		//return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
		}

		// // Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
		// return new Promise(function(resolve, reject) {
		// getUserMedia.call(navigator, constraints, resolve, reject);
		// });
		}
	}
	else
	{
		getUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator);
		console.log("Using navigator.mediaDevices.getuserMedia");
	}
	
	//getUserMedia = (navigator.getUserMedia||navigator.mozGetUserMedia||navigator.webkitGetUserMedia||navigator.mediaDevices.getUserMedia).bind(navigator);

	if(!getUserMedia)
	{
		console.log("This is not a webrtc compatible browser");
	}
	else
	{
		console.log("This is a webrtc compatible browser ");
	}

	// function hasRTCPeerConnection() {
	// 	window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
	// 	console.log("inside hasRTCPeerConnection");
	// 	return !!window.RTCPeerConnection;
	// }

	// function hasRTCSessionDescription(){
	// 	window.RTCSessionDescription = window.RTCSessionDescription || window.mozSessionDescription;
	// 	console.log("inside hasRTCSessionDescription");
	// 	return !!window.RTCSessionDescription;
	// }

	// function hasRTCIceCandidate(){
	// 	window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate;
	// 	console.log("inside hasRTCIceCandidate");
	// 	return !!window.RTCIceCandidate;
	// }

	RTCPeerConnection = RTCPeerConnection||webkitRTCPeerConnection||mozRTCPeerConnection;
	RTCSessionDescription = RTCSessionDescription||mozRTCSessionDescription;
	RTCIceCandidate = RTCIceCandidate||mozRTCIceCandidate;

	attachMediaStream = function (element, stream) {
		console.log("attaching mediastream");
		if (typeof element.srcObject != 'undefined') {
			console.log("using srcObject");
			element.srcObject = stream;
		} else if (typeof element.mozSrcObject != 'undefined') {
			console.log("using mozSrcObject");
			element.mozSrcObject = stream;
			element.play();
		} else if (typeof element.src != 'undefined') {
			console.log("using src");
			element.src = URL.createObjectURL(stream);
		} else {
			console.log('Error attaching stream to element.');
		}
		console.log("attached to mediastream");
	};
	
	detachMediaStream = function (element) {
		console.log("detaching media stream");
		element.pause();
		if (typeof element.srcObject !== 'undefined') {
			element.srcObject = null;
		} else if (typeof element.mozSrcObject !== 'undefined') {
			element.mozSrcObject = null;
		} else if (typeof element.src !== 'undefined') {
			element.src = null;
		}
		// var stream;
		// //stream = remoteVideoEl.srcObject;
		// 	if(element.srcObject){
		// 		stream = element.srcObject;
		// 	}
		// 	else if(element.mozSrcObject){
		// 		stream = element.mozSrcObject;
		// 	}
		// 	else if(element.src){
		// 		stream = element.src;
		// 	}
		// if(stream){
		// 	detachVideo(stream);
		// 	detachAudio(stream);
		// 	if (typeof element.srcObject != 'undefined') {
		// 		element.srcObject = null;
		// 		element.src = null;
		// 	} else if (typeof element.mozSrcObject != 'undefined') {
		// 		element.mozSrcObject = null;
		// 		element.pause();
		// 		element.mozSrcObject = null;
		// 	} else if (typeof element.src != 'undefined') {
		// 		element.src = null;
		// 	}
		// }
	};

	//detaches video
	detachVideo = function (stream) { // stream is your local WebRTC stream
		console.log("INSIDE DETACHVIDEO");
		var videoTracks = stream.getVideoTracks();
		for (var i = 0, l = videoTracks.length; i < l; i++) {
			videoTracks[i].enabled = false;
			videoTracks[i].stop();
			console.log("VIDEO TRACK STATE :" + videoTracks[i].enabled);
		}
	};

	//detaches audio
	detachAudio = function (stream) { // stream is your local WebRTC stream
		console.log("INSIDE DETACHAUDIO");
		var audioTracks = stream.getAudioTracks();
		for (var i = 0, l = audioTracks.length; i < l; i++) {
			audioTracks[i].enabled = false;
			audioTracks[i].stop();
			//$scope.showToggleAudioBtn = !($scope.showToggleAudioBtn);
			console.log("AUDIO TRACK STATE :" + audioTracks[i].enabled);
		}
	};

	reattachMediaStream = function (to, from) {
		console.log("Reattaching media stream");
		if(from.src !== 'undefined'){
			to.src = from.src;
		}
		else if(from.mozSrcObject !== 'undefined'){
			to.mozSrcObject = from.mozSrcObject;
		to.play();
		}
	};	

	// The representation of tracks in a stream is changed in M26.
	// Unify them for earlier Chrome versions in the coexisting period.
	if (!webkitMediaStream.prototype.getVideoTracks) {
		webkitMediaStream.prototype.getVideoTracks = function () {
			return this.videoTracks;
		};
		webkitMediaStream.prototype.getAudioTracks = function () {
			return this.audioTracks;
		};
	}

	// New syntax of getXXXStreams method in M26.
	if (!webkitRTCPeerConnection.prototype.getLocalStreams) {
		webkitRTCPeerConnection.prototype.getLocalStreams = function () {
			return this.localStreams;
		};
		webkitRTCPeerConnection.prototype.getRemoteStreams = function () {
			return this.remoteStreams;
		};
	}

	// Fake get{Video,Audio}Tracks
	if (!MediaStream.prototype.getVideoTracks) {
		MediaStream.prototype.getVideoTracks = function () {
			return [];
		}
	}

	if (!MediaStream.prototype.getAudioTracks) {
		MediaStream.prototype.getAudioTracks = function () {
			return [];
		}
	}