'use strict';
var RTCPeerConnection = RTCPeerConnection || null,
	RTCIceCandidate = RTCIceCandidate || null,
	RTCSessionDescription = RTCSessionDescription || null,
	getUserMedia = getUserMedia || null,
	attachMediaStream = null,
	detachMediaStream = null,
	reattachMediaStream = null,
	webrtcDetectedBrowser = null;


if (navigator.mozGetUserMedia) {
	console.log("This appears to be Firefox");

	webrtcDetectedBrowser = "firefox";
	RTCPeerConnection = mozRTCPeerConnection||RTCPeerConnection;
	RTCSessionDescription = mozRTCSessionDescription||RTCSessionDescription;
	RTCIceCandidate = mozRTCIceCandidate||RTCIceCandidate;

	// Get UserMedia (only difference is the prefix).
	// Code from Adam Barth.
	getUserMedia = navigator.mozGetUserMedia.bind(navigator);
	console.log("Attaching mozGetusermedia");
	attachMediaStream = function (element, stream) {
		console.log("Attaching media stream");
		if(element.mozSrcObject !== 'undefined'){
			element.mozSrcObject = stream;
		}
		else{
			element.srcObject = stream;
		}
		element.play();
	};
	
	detachMediaStream = function (element) {
		console.log("detaching media stream");
		element.pause();
		element.mozSrcObject = null;
	};

	reattachMediaStream = function (to, from) {
		console.log("Reattaching media stream");
		to.mozSrcObject = from.mozSrcObject;
		to.play();
	};

	// Fake get{Video,Audio}Tracks
	if (!MediaStream.prototype.getVideoTracks) {
		MediaStream.prototype.getVideoTracks = function () {
			return [];
		}
	}

	if (!MediaStream.prototype.getAudioTracks) {
		MediaStream.prototype.getAudioTracks = function () {
			return [];
		}
	}
} else if (navigator.webkitGetUserMedia) {
	console.log("This appears to be Chrome");

	webrtcDetectedBrowser = "chrome";

	RTCPeerConnection = webkitRTCPeerConnection;
	
	// Get UserMedia (only difference is the prefix).
	// Code from Adam Barth.
	getUserMedia = navigator.webkitGetUserMedia.bind(navigator);
	console.log("Attaching webkitgetusermedia");
	// Attach a media stream to an element.
	attachMediaStream = function (element, stream) {
		if (typeof element.srcObject !== 'undefined') {
			element.srcObject = stream;
		} else if (typeof element.mozSrcObject !== 'undefined') {
			element.mozSrcObject = stream;
		} else if (typeof element.src !== 'undefined') {
			element.src = URL.createObjectURL(stream);
		} else {
			console.log('Error attaching stream to element.');
		}
	};
	
	detachMediaStream = function (element) {
		console.log("detaching media stream");
		element.pause();
		if (typeof element.srcObject !== 'undefined') {
			element.srcObject = null;
		} else if (typeof element.mozSrcObject !== 'undefined') {
			element.mozSrcObject = null;
		} else if (typeof element.src !== 'undefined') {
			element.src = null;
		}
	};

	reattachMediaStream = function (to, from) {
		to.src = from.src;
	};

	// The representation of tracks in a stream is changed in M26.
	// Unify them for earlier Chrome versions in the coexisting period.
	if (!webkitMediaStream.prototype.getVideoTracks) {
		webkitMediaStream.prototype.getVideoTracks = function () {
			return this.videoTracks;
		};
		webkitMediaStream.prototype.getAudioTracks = function () {
			return this.audioTracks;
		};
	}

	// New syntax of getXXXStreams method in M26.
	if (!webkitRTCPeerConnection.prototype.getLocalStreams) {
		webkitRTCPeerConnection.prototype.getLocalStreams = function () {
			return this.localStreams;
		};
		webkitRTCPeerConnection.prototype.getRemoteStreams = function () {
			return this.remoteStreams;
		};
	}
} 
	else if(navigator.mediaDevices.getUserMedia){
		console.log("This appears to be webRTC compatible browser");

		webrtcDetectedBrowser = "edge";
		RTCPeerConnection = RTCPeerConnection;
		RTCSessionDescription = RTCSessionDescription;
		RTCIceCandidate = RTCIceCandidate;

		// Get UserMedia (only difference is the prefix).
		// Code from Adam Barth.
		getUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator);
		console.log("Getusermedia for edge");

		attachMediaStream = function (element, stream) {
			console.log("Attaching media stream");
			if (typeof element.srcObject !== 'undefined') {
				element.srcObject = stream;
			} else if (typeof element.mozSrcObject !== 'undefined') {
				element.mozSrcObject = stream;
			} else if (typeof element.src !== 'undefined') {
				element.src = URL.createObjectURL(stream);
			} else {
				console.log('Error attaching stream to element.');
			}
		};
	
		detachMediaStream = function (element) {
			console.log("detaching media stream");
			element.pause();
			element.srcObject = null;
		};

		reattachMediaStream = function (to, from) {
			console.log("Reattaching media stream");
			to.srcObject = from.srcObject;
			to.play();
		};

		// Fake get{Video,Audio}Tracks
		if (!MediaStream.prototype.getVideoTracks) {
			MediaStream.prototype.getVideoTracks = function () {
				return [];
			}
		}

		if (!MediaStream.prototype.getAudioTracks) {
			MediaStream.prototype.getAudioTracks = function () {
				return [];
			}
		}
	
}
else {
	console.log("Browser does not appear to be WebRTC-capable");
}

